# Global variables:
MKDIR=/bin/mkdir
INSTALL=/usr/bin/install
CP=/bin/cp
CONFIGDIR=/etc/awsr2c/
LOGDIR=/var/log/awsr2c/
APACHECONFDIR=/etc/httpd/conf.d/
INITDIR=/usr/lib/systemd/system
SCRIPTDIR=/usr/bin

# Note this is where we do it for centos7.  YMMV!
PYTHON_SITEPACKAGES=/usr/lib/python2.7/site-packages/

PROJECT=managedrecovery

# set the default rule:
.DEFAULT_GOAL=stage

#Get the timestamp for the date
ITER:=$(shell date +%Y%m%d%H%M%s)
#Get the home directory
HOMEDIR:=$(shell git rev-parse --show-toplevel)
#Get the version from the release file in ROOT
VERSION:=$(shell cat $(HOMEDIR)/RELEASE)
yumrepo=/var/www/yumrepo/managedrecovery/releases/$(VERSION)/
ROOT:=$(shell mktemp -d)
SUBPROJECT:=$(shell pwd | sed 's,.*/,,1' )
PYTHONDIR:=$(shell ls -d python)

# Embed the call to setup.py in the makefile:
define SETUP_PY =
import os; \
os.environ['PYTHONPATH']= '$(ROOT)$(PYTHON_SITEPACKAGES)/'; \
from setuptools import setup, find_packages; \
packages = [ '$(PROJECT).' + i for i in find_packages('$(PROJECT)')]; \
setup(name='$(PACKAGE_NAME)', \
      version='$(VERSION)', \
      author='Makefile Robot', \
      author_email='nobody@sungardas.com', \
      license='Sungard AS Proprietary', \
      namespace_packages = ['$(PROJECT)', '$(PROJECT).$(SUBPROJECT)'], \
      packages=packages, \
      )
endef

stage: setup build

setup:
	if [ "$(PYTHONDIR)" = "python" ]; then \
		mkdir -p $(ROOT)/$(PYTHON_SITEPACKAGES); \
		cd python && python -c "$(SETUP_PY)" install --prefix="$(ROOT)/usr" --old-and-unmanageable; \
	fi

#build the rpm
buildrpm:stage
	fpm -t rpm --version $(VERSION) --iteration $(ITER) -n $(PACKAGE_NAME) $(foreach dep,$(DEPENDENCIES),-d $(dep)) -s dir -C $(ROOT) `cd $(ROOT) && ls -d */`
	pip install $(foreach dep,$(PIP_DEPENDENCIES),$(dep))
	sudo rm -rf $(ROOT)
#install the rpm in a repo server
installrpm:buildrpm
	sudo mkdir -p $(yumrepo)
	sudo mv *rpm $(yumrepo)
	sudo createrepo $(yumrepo)
	sudo chmod -R g+w $(yumrepo)
	sudo rm -rf python/build python/r2c_*.egg-info

# install the rpm.  Intended when building and installing on same machine (local testing):
install:buildrpm
	sudo yum remove -y $(PACKAGE_NAME) && sudo yum install -y ./$(PACKAGE_NAME)-$(VERSION)-$(ITER).x86_64.rpm
	sudo rm -f ./$(PACKAGE_NAME)-$(VERSION)-$(ITER).x86_64.rpm

test:
	# NOTE, this might need to be expanded to support coverage
	cd $(TEST_DIR) && nosetests

installscripts: scriptssetup build
	fpm -t rpm --version $(VERSION) --iteration $(ITER) -n $(PACKAGE_NAME) $(foreach dep,$(DEPENDENCIES),-d $(dep)) -s dir -C $(ROOT) `cd $(ROOT) && ls -d */`
	pip install $(foreach dep,$(PIP_DEPENDENCIES),$(dep))
	sudo rm -rf $(ROOT)
	sudo mkdir -p $(yumrepo)
	sudo mv *rpm $(yumrepo)
	sudo createrepo $(yumrepo)
	sudo chmod -R g+w $(yumrepo)

scriptssetup:
	mkdir -p $(ROOT)

