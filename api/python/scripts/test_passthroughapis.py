'''
Created on Jul 22, 2015

@author: sonal.ojha
'''
from managedrecovery.api.interfaces.base.baseHttp import BaseHttpInterface
import json


if __name__ == "__main__":
    interface = BaseHttpInterface()
    interface.host = "54.67.79.85"
#     interface.host = "192.168.10.51"
    interface.base_url = "http://%s/api/1.0" % interface.host

    params = {"username": "admin", "password": "admin",
              "vendorkey": "1950-4D70-2506-0A43-135B-5727-1A3F"}
    session_id = interface.get("login/", params)
    print session_id

    # list all hosts
    params = {"sessionid": session_id}
    hosts = interface.get("hosts/", params)
    print hosts

    # prepare a host
    body = dict()
    body["ipaddress"] = "52.8.93.232"
    body["username"] = "ec2-user"
    body["pkey_path"] = "/etc/awsr2c/SonalRHEL7.pem"
    body["env"] = "Linux"
    body["sky_ip"] = "52.8.201.126"
    res = interface.post("hosts/prepare/", {}, body=json.dumps(body))
    print res

    # add host
    body = dict()
    body["sessionid"] = session_id
    body["hostname"] = "ip-10-10-100-9"
    body["ipaddress"] = "10.10.100.9"
    body["iscsiname"] = res.get("iqnname")
    body["hosttype"] = ""
    addhost_resp = interface.post("hosts/", {},
                                  body=json.dumps(body))
    print addhost_resp

    # protect a host
    body = {"sessionid": session_id, "hostid": addhost_resp,
            "slt": "Tier-4 - Local Protection Only", "slp": "LocalProfile"}
    p_response = interface.post("hosts/protect/", {}, body=json.dumps(body))
    print p_response

    # unprotect a host
    params = {"sessionid": session_id, "host_id": addhost_resp}
    interface.delete("hosts/unprotect/", params)

    # list applications
    params = {"sessionid": session_id, "host_id": addhost_resp}
    apps = interface.get("hosts/applications/", params)
    print apps

    apps = [app for app in apps if app.get("hostid") == addhost_resp]

    # protect an app
    body = dict()
    body["sessionid"] = session_id
    body["app_id"] = apps[0].get("id")
    body["slt"] = "Tier-4 - Local Protection Only"
    body["slp"] = "LocalProfile"
    body["description"] = "testing"
    protect_app = interface.post("hosts/applications/protect/", {},
                                 body=json.dumps(body))
    print protect_app

    # unprotect an app
    params = dict()
    params["sessionid"] = session_id
    params["app_id"] = apps[0].get("id")
    unprotect_app = interface.delete("hosts/applications/unprotect/", params)
    print unprotect_app

    # create a group
    body = dict()
    body["sessionid"] = session_id
    body["group_name"] = "TestGroup3"
    body["description"] = "testing"
    creat_grp_id = interface.post("applications/groups/", {},
                                  body=json.dumps(body))
    print creat_grp_id

    # list groups
    params = dict()
    params["sessionid"] = session_id
    grups = interface.get("applications/groups/", params)
    print grups

    # list group members
    params = dict()
    params["sessionid"] = session_id
    members = interface.get("applications/groups/members/", params)
    print members

    # add a group member
    body = dict()
    body["sessionid"] = session_id
    body["group_id"] = creat_grp_id
    body["app_id"] = apps[0].get("id")
    add_member_id = interface.post("applications/groups/members/", {},
                                   body=json.dumps(body))
    print add_member_id

    # delete a group member
    params = dict()
    params["sessionid"] = session_id
    params["group_member_id"] = add_member_id
    delete = interface.delete("applications/groups/members/", params)
    print delete

    # add a group member
    body = dict()
    body["sessionid"] = session_id
    body["group_id"] = creat_grp_id
    body["app_id"] = apps[0].get("id")
    add_member_id = interface.post("applications/groups/members/", {},
                                   body=json.dumps(body))
    print add_member_id

    # protect a group
    body = dict()
    body["sessionid"] = session_id
    body["group_id"] = creat_grp_id
    body["slt"] = "Tier-4 - Local Protection Only"
    body["slp"] = "LocalProfile"
    body["description"] = "testing"
    protect = interface.post("applications/groups/protect/", {},
                             body=json.dumps(body))
    print protect

    # unprotect a group
    params = dict()
    params["sessionid"] = session_id
    params["group_id"] = creat_grp_id
    delete = interface.delete("applications/groups/unprotect/", params)
    print delete

    # delete group
    params = dict()
    params["sessionid"] = session_id
    params["group_id"] = creat_grp_id
    grups = interface.delete("applications/groups/", params)
    print grups
