'''
Created on Jul 6, 2015

@author: sonal.ojha
'''
from managedrecovery.api.interfaces.base.baseHttp import BaseHttpInterface
import json


if __name__ == "__main__":
    interface = BaseHttpInterface()
    interface.host = "54.67.79.85"
#     interface.host = "192.168.10.51"
    interface.base_url = "http://%s/api/1.0" % interface.host

    # prepare a host
    body = dict()
    body["ipaddress"] = "52.8.93.232"
    body["username"] = "ec2-user"
    body["pkey_path"] = "/etc/awsr2c/SonalRHEL7.pem"
    body["env"] = "Linux"
    body["sky_ip"] = "52.8.201.126"
    res = interface.post("hosts/prepare/", {}, body=json.dumps(body))
    print res

    params = {"username": "admin", "password": "admin",
              "vendorkey": "1950-4D70-2506-0A43-135B-5727-1A3F"}
    session_id = interface.get("login/", params)
    print session_id

    # register a host, rhel 7.1
    body = dict()
    body["sessionid"] = session_id
    body["hostname"] = "ip-10-10-100-9"
    body["ipaddress"] = "10.10.100.9"
    body["iqnname"] = res.get("iqnname")
    body["hosttype"] = ""
    body["test_add_host"] = True
    rhel_response = interface.post("hosts/register/", {},
                                   body=json.dumps(body))
    print rhel_response

    # create a protection domain
    body = {"sessionid": session_id, "domain_name": "TestGroup2",
            "description": "This is a test group",
            "hostid": rhel_response.get("hostid"),
            "slt": "Tier-4 - Local Protection Only", "slp": "LocalProfile"}
    pd_response = interface.post("protectiondomains/", {},
                                 body=json.dumps(body))
    print pd_response

    # get protection domain details
    group_id = pd_response.get("protectiondomain_id")
    print interface.get("protectiondomains/%s" % group_id,
                        {"sessionid": session_id})

    # prepare a host
    body = dict()
    body["ipaddress"] = "52.8.128.181"
    body["username"] = "Administrator"
    body["password"] = "Sungard09"
    body["env"] = "Windows"
    body["sky_ip"] = "52.8.201.126"
    res = interface.post("hosts/prepare/", {}, body=json.dumps(body))
    print res

    # register a host, windows 2012
    body = dict()
    body["sessionid"] = session_id
    body["hostname"] = "ip-10-10-100-238"
    body["ipaddress"] = "10.10.100.238"
    body["iqnname"] = res.get("iqnname")
    body["hosttype"] = ""
    body["test_add_host"] = True
    win_response = interface.post("hosts/register/", {}, body=json.dumps(body))
    print win_response

    # add windows 2012 machine to protection domain
    body = dict()
    body["sessionid"] = session_id
    body["domain_name"] = "TestGroup2"
    body["hostid"] = win_response.get("hostid")
    add_member_response = interface.post("protectiondomains/members/", {},
                                         body=json.dumps(body))
    print add_member_response

    # delete windows 2012 machine from protection domain
    print add_member_response.get("members")
    protectiondomain_name = add_member_response.get("domain_name")
    print protectiondomain_name
    memberid = add_member_response.get("members")[0].get("member_hostid")
    url = "protectiondomains/members/%s" % memberid
    params = {"sessionid": session_id,
              "protectiondomain_name": protectiondomain_name}
    add_member_response = interface.delete(url, params)
    print add_member_response
    protectiondomain_name = "TestGroup2"

    # delete rhel machine from protection domain
    print pd_response.get("members")
    memberid = pd_response.get("members")[0].get("member_hostid")
    url = "protectiondomains/members/%s" % memberid
    params = {"sessionid": session_id,
              "protectiondomain_name": protectiondomain_name}
    add_member_response = interface.delete(url, params)
    print add_member_response

    # unregister a host, rhel 7.1
    body = dict()
    body["sessionid"] = session_id
    body["hostid"] = rhel_response.get("hostid")
    body["ipaddress"] = "52.8.93.232"
    body["username"] = "ec2-user"
    body["env"] = "Linux"
    body["pkey_path"] = "/etc/awsr2c/SonalRHEL7.pem"
    res = interface.post("hosts/deregister/", {}, body=json.dumps(body))
    print res

    # unregister a host, windows 2012
    body = dict()
    body["sessionid"] = session_id
    body["hostid"] = win_response.get("hostid")
    body["ipaddress"] = "52.8.128.181"
    body["username"] = "Administrator"
    body["env"] = "Windows"
    body["password"] = "Sungard09"

    res = interface.post("hosts/deregister/", {}, body=json.dumps(body))
    print res
