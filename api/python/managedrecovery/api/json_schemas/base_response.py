base_response = {
    "id": "base_response",
    "name": "base_response",
    "description": "Base API response envelope which other response objects extend",
    "type": "object",
    "properties": {
        "status": {
            "type": "string"
        },
        "message": {
            "type": "string"
        },
        "results": {
            "type": "any"
        },
        "error": {
            "type": "any"
        }
    }
}
