'''
Created on May 27, 2015

@author: sonal.ojha
'''

protected_host = {
    "title": "Protecthost",
    "$schema": "http://json-schema.org/draft-03/schema",
    "id": "http://jsonschema.net",
    "type": "object",
    "properties": {
        "sessionid": {
            "type": "string",
            "required": True,
            "description": "UUID returned on successful login into Sky"
        },
        "hostid": {
            "type": "string",
            "required": True,
            "description": "Unique ID of the host"
        },
        "slp": {
            "type": "string",
            "required": True,
            "description": "slp"
        },
        "slt": {
            "type": "string",
            "required": True,
            "description": "slt"
        }
    }
}

register_host_response = {
    "title": "RegisterHostResponse",
    "$schema": "http://json-schema.org/draft-03/schema",
    "id": "http://jsonschema.net",
    "type": "object",
    "properties": {
        "hostid": {
            "type": "string",
            "required": True,
            "description": "Unique id of the host"
        },
        "test_iscsi_status": {
            "type": "string",
            "required": True,
            "description": "success or failure of the test"
        }
    }
}

register_host = {
    "title": "RegisterHost",
    "$schema": "http://json-schema.org/draft-03/schema",
    "id": "http://jsonschema.net",
    "type": "object",
    "properties": {
        "sessionid": {
            "type": "string",
            "required": True,
            "description": "UUID returned on successful login into Sky"
        },
        "hostname": {
            "type": "string",
            "required": True,
            "description": "host name"
        },
        "ipaddress": {
            "type": "string",
            "required": True,
            "description": "Ip Address of the host machine"
        },
        "iqnname": {
            "type": "string",
            "required": True,
            "description": "IQN unique name"
        },
        "hosttype": {
            "type": "string",
            "required": False,
            "description": "host type"
        }
    }
}

unregister_host = {
    "title": "UnRegisterHost",
    "$schema": "http://json-schema.org/draft-03/schema",
    "id": "http://jsonschema.net",
    "type": "object",
    "properties": {
        "sessionid": {
            "type": "string",
            "required": True,
            "description": "UUID returned on successful login into Sky"
        },
        "hostid": {
            "type": "string",
            "required": False,
            "description": "Unique id of the host"
        },
        "hostname": {
            "type": "string",
            "required": False,
            "description": "Hostname of the host"
        }
    }
}

unregister_host_response = {
    "title": "RegisterHostResponse",
    "$schema": "http://json-schema.org/draft-03/schema",
    "id": "http://jsonschema.net",
    "type": "object",
    "properties": {
        "status": {
            "type": "string",
            "required": True,
            "description": "Whether the cleanup was success or failure"
        }
    }
}

prepare_host = {
    "title": "PrepareHost",
    "$schema": "http://json-schema.org/draft-03/schema",
    "id": "http://jsonschema.net",
    "type": "object",
    "properties": {
        "ipaddress": {
            "type": "string",
            "required": True,
            "description": "Ip Address of the host machine"
        },
        "username": {
            "type": "string",
            "required": True,
            "description": "host username"
        },
        "password": {
            "type": "string",
            "required": False,
            "description": "host password"
        },
        "env": {
            "type": "string",
            "required": True,
            "description": "If the host is linux or windows"
        },
        "pkey_path": {
            "type": "string",
            "required": False,
            "description": "Path to key file, only for linux machines"
        },
        "sky_ip": {
            "type": "string",
            "required": True,
            "description": "IP of the SKY machine"
        }
    }
}


prepare_host_response = {
    "title": "PrepareHostResponse",
    "$schema": "http://json-schema.org/draft-03/schema",
    "id": "http://jsonschema.net",
    "type": "object",
    "properties": {
        "iqnname": {
            "type": "string",
            "required": True,
            "description": "ISCSI qualified name"
        }
    }
}