'''
Created on May 27, 2015

@author: sonal.ojha
'''

create_protection_domain = {
    "title": "CreateProtectionDomain",
    "$schema": "http://json-schema.org/draft-03/schema",
    "id": "http://jsonschema.net",
    "type": "object",
    "properties": {
        "sessionid": {
            "type": "string",
            "required": True,
            "description": "Unique UID of the sky session"
        },
        "domain_name": {
            "type": "string",
            "required": True,
            "description": "Name of the protection domain to be created"
        },
        "description": {
            "type": "string",
            "required": True,
            "description": "Additional description for the group (optional)"
        },
        "hostid": {
            "type": "string",
            "required": True,
            "description": "Unique ID of the host"
        },
        "slt": {
            "type": "string",
            "required": True,
            "description": "Unique name of the Sky service level template"
        },
        "slp": {
            "type": "string",
            "required": False,
            "description": "Unique name of the Sky service level policy"
        }
    }
}

create_protection_domain_response = {
    "title": "CreateProtectionDomainResponse",
    "$schema": "http://json-schema.org/draft-03/schema",
    "id": "http://jsonschema.net",
    "type": "object",
    "properties": {
        "protectiondomain_id": {
            "type": "string",
            "required": True,
            "description": "Unique ID of the protection domain"
        },
        "members": {
            "type": "array",
            "required": True,
            "description": "List of members of the protection domain",
            "items": {
                "properties": {
                    "member_hostid": {
                        "type": "string",
                        "required": True,
                        "description": "Unique hostID of the protection domain member"
                    }
                }
            }
        }
    }
}

get_protection_domain_response = {
    "title": "GetProtectionDomainResponse",
    "$schema": "http://json-schema.org/draft-03/schema",
    "id": "http://jsonschema.net",
    "type": "array",
    "items": {
        "properties": {
            "member_hostname": {
                "type": "string",
                "required": True,
                "description": "Host name of the protection domain member"
            },
            "member_hostid": {
                "type": "string",
                "required": True,
                "description": "Unique hostID of the protection domain member"
            }
        }
    }
}

