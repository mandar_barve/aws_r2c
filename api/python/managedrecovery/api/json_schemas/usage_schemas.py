'''
Created on July 24, 2015

@author: chandra.mishra
'''

get_aggregate_diskusage_response = {
    "title": "GetAggregateDiskUsageResponse",
    "$schema": "http://json-schema.org/draft-03/schema",
    "id": "http://jsonschema.net",
    "type": "array",
    "items": {
        "properties": {
            "used": {
                "type": "string",
                "required": True,
                "description": "total disk used in GB"
            },
            "capacity": {
                "type": "string",
                "required": True,
                "description": "total disk capacity in GB"
            },
            "poolname": {
                "type": "string",
                "required": True,
                "description": "name of the pool"
            },
            "free": {
                "type": "string",
                "required": True,
                "description": "total free disk in GB"
            },
            "stattime": {
                "type": "string",
                "required": True,
                "description": "date(yyyy-mm-dd)"
            },
            "used(%)": {
                "type": "string",
                "required": True,
                "description": "total used disk in percentage"
            }
        }
    }
}
get_diskusage_response = {
    "title": "GetAggregateDiskUsageResponse",
    "$schema": "http://json-schema.org/draft-03/schema",
    "id": "http://jsonschema.net",
    "type": "array",
    "items": {
        "properties": {
            "used": {
                "type": "string",
                "required": True,
                "description": "total disk used in GB"
            },
            "capacity": {
                "type": "string",
                "required": True,
                "description": "total disk capacity in GB"
            },
            "poolname": {
                "type": "string",
                "required": True,
                "description": "name of the pool"
            },
            "free": {
                "type": "string",
                "required": True,
                "description": "total free disk in GB"
            },
            "stattime": {
                "type": "string",
                "required": True,
                "description": "date(yyyy-mm-dd)"
            },
            "used(%)": {
                "type": "string",
                "required": True,
                "description": "total used disk in percentage"
            }
        }
    }
}

get_dedupusage_response = {
    "title": "GetDedupUsageResponse",
    "$schema": "http://json-schema.org/draft-03/schema",
    "id": "http://jsonschema.net",
    "type": "array",
    "items": {
        "properties": {
            "newsize": {
                "type": "string",
                "required": True,
                "description": "new disk size of the app"
            },
            "appid": {
                "type": "string",
                "required": True,
                "description": "unique id of app"
            },
            "hostname": {
                "type": "string",
                "required": True,
                "description": "name of the host"
            },
            "dedupsize": {
                "type": "string",
                "required": True,
                "description": "size of dedupe"
            },
            "stattime": {
                "type": "string",
                "required": True,
                "description": "date(yyyy-mm-dd)"
            },
            "used(%)": {
                "type": "string",
                "required": True,
                "description": "total used disk in percentage"
            }
        }
    }
}
get_snapshotusage_response = {
    "title": "GetSnapshotUsageResponse",
    "$schema": "http://json-schema.org/draft-03/schema",
    "id": "http://jsonschema.net",
    "type": "array",
    "items": {
        "properties": {
            "vdiskcount": {
                "type": "string",
                "required": True,
                "description": "number of vdisk"
            },
            "appname": {
                "type": "string",
                "required": True,
                "description": "name of the app"
            },
            "hostname": {
                "type": "string",
                "required": True,
                "description": "name of the host"
            },
            "totalused": {
                "type": "string",
                "required": True,
                "description": "total disk used"
            },
            "stattime": {
                "type": "string",
                "required": True,
                "description": "date(yyyy-mm-dd)"
            },
            "appid": {
                "type": "string",
                "required": True,
                "description": "unique id of app"
            },
            "appsize": {
                "type": "string",
                "required": True,
                "description": "size of app"
            },
            "totalstaging": {
                "type": "string",
                "required": True,
                "description": "size of totalstaging"
            }
        }
    }
}
get_mdlusage_response = {
    "title": "GetMdlUsageResponse",
    "$schema": "http://json-schema.org/draft-03/schema",
    "id": "http://jsonschema.net",
    "type": "array",
    "items": {
        "properties": {
            "hostname": {
                "type": "string",
                "required": True,
                "description": "name of the host"
            },
            "appname": {
                "type": "string",
                "required": True,
                "description": "name of the app"
            },
            "appid": {
                "type": "string",
                "required": True,
                "description": "unique id of the app"
            },
            "manageddata": {
                "type": "string",
                "required": True,
                "description": "manageddata disk size"
            },
            "allocated": {
                "type": "string",
                "required": True,
                "description": "unique id of app"
            },
            "appsize": {
                "type": "string",
                "required": True,
                "description": "name of the host"
            },
            "capacity": {
                "type": "string",
                "required": True,
                "description": "size of dedupe"
            },
            "stattime": {
                "type": "string",
                "required": True,
                "description": "date(yyyy-mm-dd)"
            }
        }
    }
}

get_aggregate_mdlusage_response = {
    "title": "GetAggregateMdlUsageResponse",
    "$schema": "http://json-schema.org/draft-03/schema",
    "id": "http://jsonschema.net",
    "type": "array",
    "items": {
        "properties": {
            "manageddata": {
                "type": "string",
                "required": True,
                "description": "manageddata disk size"
            },
            "allocated": {
                "type": "string",
                "required": True,
                "description": "unique id of app"
            },
            "appsize": {
                "type": "string",
                "required": True,
                "description": "name of the host"
            },
            "capacity": {
                "type": "string",
                "required": True,
                "description": "size of dedupe"
            },
            "stattime": {
                "type": "string",
                "required": True,
                "description": "date(yyyy-mm-dd)"
            }
        }
    }
}

get_backupusage_response = {
    "title": "GetAggregateMdlUsageResponse",
    "$schema": "http://json-schema.org/draft-03/schema",
    "id": "http://jsonschema.net",
    "type": "array",
    "items": {
        "properties": {
            "status": {
                "type": "string",
                "required": True,
                "description": "Status of this backup image. Status like succeeded, failed, running, etc"
            },
            "backupname": {
                "type": "string",
                "required": True,
                "description": "Image name"
            },
            "virtualsize": {
                "type": "string",
                "required": True,
                "description": "Backup object application size"
            },
            "mountedhost": {
                "type": "string",
                "required": True,
                "description": "Id of host where backup image is mounted"
            },
            "modifydate": {
                "type": "string",
                "required": True,
                "description": "Date when backup image is last modified"
            },
            "appname": {
                "type": "string",
                "required": True,
                "description": "Name of the application"
            },
            "hostname": {
                "type": "string",
                "required": True,
                "description": "Host name of backup image where application was running"
            },
            "backupdate": {
                "type": "string",
                "required": True,
                "description": "Start date"
            },
            "expiration": {
                "type": "string",
                "required": True,
                "description": "Expiration date time when this should expire"
            },
            "appid": {
                "type": "string",
                "required": True,
                "description": "Application object id"
            },
            "jobclass": {
                "type": "string",
                "required": True,
                "description": "Type of the job that created this backup image."
            },
            "consistencydate": {
                "type": "string",
                "required": True,
                "description": "Application consistency timestamp"
            }
        }
    }
}
get_protect_host_response = {
    "title": "GetProtectHostResponse",
    "$schema": "http://json-schema.org/draft-03/schema",
    "id": "http://jsonschema.net",
    "type": "array",
    "items": {
        "properties": {
            "": {
                "hostname": "string",
                "required": True,
                "description": "hostname of protected vm"
            },
            "ostype": {
                "type": "string",
                "required": True,
                "description": "type of os to protected vm"
            },
            "installeddate": {
                "type": "string",
                "required": True,
                "description": "add host date"
            },
            "modifydate": {
                "type": "string",
                "required": True,
                "description": "modifydate"
            },
            "ipaddress": {
                "type": "string",
                "required": True,
                "description": "ip address of protected host"
            }
        }
    }
}
get_protect_host_count_response = {
    "title": "GetProtectHostResponse",
    "$schema": "http://json-schema.org/draft-03/schema",
    "id": "http://jsonschema.net",
    "type": "array",
    "items": {
        "properties": {
            "": {
                "total number of protected vm": "string",
                "required": True,
                "description": "total number of protected host"
            },
            "": {
                "total number of protected app": "string",
                "required": True,
                "description": "total number of protected app"
            }
        }
    }
}
get_total_protect_appsize_response = {
    "title": "GetTotalProtectionAppSizeResponse",
    "$schema": "http://json-schema.org/draft-03/schema",
    "id": "http://jsonschema.net",
    "type": "array",
    "items": {
        "properties": {
            "": {
                "total protection app size": "string",
                "required": True,
                "description": "hostname of protected vm"
            },
            "stattime": {
                "type": "string",
                "required": True,
                "description": "report generation date"
            }
        }
    }
}
