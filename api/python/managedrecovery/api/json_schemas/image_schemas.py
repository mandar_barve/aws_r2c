'''
Created on Aug 7, 2015

@author: sonal.ojha
'''


mount_image_response = {
    "title": "MountImageResponse",
    "$schema": "http://json-schema.org/draft-03/schema",
    "id": "http://jsonschema.net",
    "type": "object",
    "properties": {
        "mount_status": {
            "type": "string",
            "required": True,
            "description": "Success / Failure for the mount of an image"
        },
        "failback_status": {
            "type": "string",
            "required": False,
            "description": "Success / Failure for the failback of host applications"
        },
        "protection_status": {
            "type": "string",
            "required": False,
            "description": "Success / Failure for the failback of host applications"
        }
    }
}

mount_image = {
    "title": "MountImage",
    "$schema": "http://json-schema.org/draft-03/schema",
    "id": "http://jsonschema.net",
    "type": "object",
    "properties": {
        "sessionid": {
            "type": "string",
            "required": True,
            "description": "UUID returned on successful login into Sky"
        },
        "hostname": {
            "type": "string",
            "required": True,
            "description": "Hostname of the host whose image has to be mounted"
        },
        "new_hostname": {
            "type": "string",
            "required": True,
            "description": "Hostname of the host on which the image has to be mounted"
        },
        "label": {
            "type": "string",
            "required": True,
            "description": "Label for the mounted image."
        },
        "protect_new_host": {
            "type": "boolean",
            "required": True,
            "description": "Whether the new host has to be protected with the slt and slp applied on the original host"
        }
    }
}

restore_image_response = {
    "title": "RestoreImageResponse",
    "$schema": "http://json-schema.org/draft-03/schema",
    "id": "http://jsonschema.net",
    "type": "object",
    "properties": {
        "restore_status": {
            "type": "string",
            "required": True,
            "description": "Success / Failure for the restore of an image"
        },
        "failback_status": {
            "type": "string",
            "required": False,
            "description": "Success / Failure for the failback of host applications"
        }
    }
}

restore_image = {
    "title": "RestoreImage",
    "$schema": "http://json-schema.org/draft-03/schema",
    "id": "http://jsonschema.net",
    "type": "object",
    "properties": {
        "sessionid": {
            "type": "string",
            "required": True,
            "description": "UUID returned on successful login into Sky"
        },
        "hostname": {
            "type": "string",
            "required": True,
            "description": "Hostname of the host whose image has to be mounted"
        }
    }
}
