'''
Created on May 27, 2015

@author: sonal.ojha
'''

# This is needed for RAML documentation

create_protection_domain_response_ex = {
    "protectiondomain_id": "1223",
    "members": [
        {
            "member_hostid": "1465"
        }
    ]
}

get_protection_domain_response_ex = [
    {
        'member_hostname': 'ip-10-10-100-238',
        'member_hostid': '499136'
    }
]

create_protection_domain_ex = {
    "sessionid": "fa2a3fc0-f79a-4da9-925c-53fe10bc0019",
    "description": "This is a group to protect all application of a host",
    "hostid": "12345",
    "slt": "Tier-4 - Local Protection Only",
    "slp": "LocalProfile",
    "name": "Test Group"
}

register_host_ex = {
    "sessionid": "fa2a3fc0-f79a-4da9-925c-53fe10bc0019",
    "ipaddress": "10.10.100.9",
    "hostname": "WIN-54OR0LCM10G",
    "hosttype": "",
    "sky_ip": "54.9.0.0",
    "iqnname": "iqn.1994-05.com.redhat:81a1e6f3c78"
}

protect_host_ex = {
    "sessionid": "fa2a3fc0-f79a-4da9-925c-53fe10bc0019",
    "hostid": "12345",
    "slp": "LocalProfile",
    "slt": "Tier-4 - Local Protection Only"
}

register_host_response_ex = {
    'hostid': '480210'
}

unregister_host_response_ex = {
    'status': True,
}


unregister_host_ex = {
    "sessionid": "fa2a3fc0-f79a-4da9-925c-53fe10bc0019",
    "hostid": "1234"
}

prepare_win_host_ex = {
    "env": "windows",
    "username": "Administrator",
    "password": "VD*2sMPX&d",
    "ipaddress": "10.10.100.9",
    "sky_ip": "54.9.0.0"
}

prepare_lin_host_ex = {
    "env": "windows",
    "username": "ec2-user",
    "ipaddress": "10.10.100.9",
    "pkey_path": "/root/key.pem",
    "sky_ip": "54.9.0.0"
}

prepare_host_response_ex = {
    "iqnname": "iqn.1994-05.com.redhat:942901ec54e"
}

add_protection_domain_mem_ex = {
    "sessionid": "fa2a3fc0-f79a-4da9-925c-53fe10bc0019",
    "hostid": ["12345"],
    "domain_name": "Test Group"
}

add_protection_domain_mem_response_ex = {
    "protectiondomain_id": "1223",
    "protectiondomain_name": "Test Group",
    "members": [
        {
            "member_hostid": "1465",
            "member_hostname": "WIN-54OR0LCM10G"
        }
    ]
}

delete_protection_domain_mem_response_ex = {
    "protectiondomain_name": "Test Group",
    "protectiondomain_id": "1234",
    "members": [
        {
            "member_hostname": "WIN-54OR0LCM10G",
            "member_hostid": "1465"
        }
    ]
}

get_aggregate_diskusage_response_ex = [
    {
        'used': '26.8740 GB',
        'capacity': '26.8740 GB',
        'poolname': 'act_ded_pool000',
        'free': '836.73 GB',
        'used(%)': '12%',
        'stattime': '2015-07-24 03:00:00.091'
    }
]

get_diskusage_response_ex = [
    {
        'used': '26.8740 GB',
        'capacity': '26.8740 GB',
        'poolname': 'act_ded_pool000',
        'free': '836.73 GB',
        'used(%)': '12%',
        'stattime': '2015-07-24 03:00:00.091'
    }
]

get_mdlusage_response_ex = [
    {
        'manageddata': '26.8740 GB',
        'allocated': '26.8740 GB',
        'capacity': '5',
        'appsize': '26.8740 GB',
        'stattime': '2015-07-24 03:00:00.091',
        'appname': '/',
        'hostname': 'ip-10-10-100-9',
        'appid': '123'
    }
]

get_aggregate_mdlusage_response_ex = [
    {
        'manageddata': '26.8740 GB',
        'allocated': '26.8740 GB',
        'capacity': '5',
        'appsize': '26.8740 GB',
        'stattime': '2015-07-24 03:00:00.091'
    }
]

get_dedupusage_response_ex = [
    {
        'appid': '123',
        'newsize': '26.8740 GB',
        'appname': '5',
        'dedupsize': '26.8740 GB',
        'hostname': 'ip-10-10-100-9',
        'totalappsize': '26.8740 GB',
        'dedupcount': '2',
        'compresssize': '26.8740 GB',
        'appsize': '26.8740 GB',
        'dedupusage': '26.8740 GB',
        'stattime': '2015-07-24 03:00:00.091'
    }
]

get_snapshotusage_response_ex = [
    {
        'vdiskcount': '2',
        'appname': '/',
        'hostname': 'WIN-54OR0LCM10G',
        'totalused': '26.8740 GB',
        'appid': "123",
        'appsize': '26.8740 GB',
        'totalstaging': '26.8740 GB',
        'stattime': '2015-07-24 03:00:00.091'
    }
]

get_backupusage_response_ex = [
    {
        'status': 'succeeded',
        'backupname': 'Image_0606871',
        'virtualsize': '11.9961 GB',
        'mountedhost': '0',
        'modifydate': '2015-07-22 08:21:27.884',
        'appname': '/',
        'hostname': 'ip-10-10-100-9',
        'backupdate': '2015-07-22 08:21:27.884',
        'expiration': '2015-07-22 08:21:27.884',
        'appid': '606827',
        'jobclass': 'dedup',
        'consistencydate': '2015-07-22 08:19:41.000'
    }
]

mount_image_response_ex = {
    "mount_status": "Success",
    "failback_status": "Success"
}

mount_image_ex = {
    "sessionid": "fa2a3fc0-f79a-4da9-925c-53fe10bc0019",
    "hostname": "ASh-prvsn02-AppVM",
    "new_hostname": "ASh-prvsn02-NEWVM",
    "label": "Label for the image"
}

restore_image_response_ex = {
    "restore_status": "Success",
    "failback_status": "Success"
}

restore_image_ex = {
    "sessionid": "fa2a3fc0-f79a-4da9-925c-53fe10bc0019",
    "hostname": "ASh-prvsn02-AppVM"
}

get_protect_host_response_ex = [
    {
        "hostname": "ip-10-10-100-218",
        "ostype": "Linux",
        "installeddate": "2015-07-22 08:19:41.000",
        "modifydate": "2015-07-22 08:19:41.000",
        "ipaddress": "10.10.100.218"
    }
]

get_protect_host_count_response_ex = [
    {
        "total number of protected vm": "4",
        "total number of protected app": "4"
    }
]

get_total_protect_appsize_response_ex = [
    {
        "total protection app size": "1.2 GB",
        "stattime": "2015-07-22 08:19:41.000"
    }
]
