'''
Created on May 27, 2015

@author: sonal.ojha
'''

add_protection_domain_mem = {
    "title": "AddProtectionDomainMember",
    "$schema": "http://json-schema.org/draft-03/schema",
    "id": "http://jsonschema.net",
    "type": "object",
    "properties": {
        "sessionid": {
            "type": "string",
            "required": True,
            "description": "UUID returned on successful login into Sky"
        },
        "domain_name": {
            "type": "string",
            "required": True,
            "description": "Name of the Protection Domain"
        },
        "hostid": {
            "type": "string",
            "required": False,
            "description": "Unique ID of the host to be added to ProtectionDomain"
        }
    }
}

add_protection_domain_mem_response = {
    "title": "CreateProtectionDomainResponse",
    "$schema": "http://json-schema.org/draft-03/schema",
    "id": "http://jsonschema.net",
    "type": "object",
    "properties": {
        "protectiondomain_id": {
            "type": "string",
            "required": True,
            "description": "Unique ID of the protection domain"
        },
        "protectiondomain_name": {
            "type": "string",
            "required": True,
            "description": "Name of the protection domain"
        },
        "members": {
            "type": "array",
            "required": True,
            "description": "List of members of the group",
            "items": {
                "properties": {
                    "member_hostname": {
                        "type": "string",
                        "required": False,
                        "description": "Protection domain member hostname"
                    },
                    "member_hostid": {
                        "type": "string",
                        "required": True,
                        "description": "Unique hostID of the protection domain member"
                    }
                }
            }
        }
    }
}

delete_protection_domain_mem_response = {
    "title": "CreateProtectionDomainResponse",
    "$schema": "http://json-schema.org/draft-03/schema",
    "id": "http://jsonschema.net",
    "type": "object",
    "properties": {
        "protectiondomain_id": {
            "type": "string",
            "required": True,
            "description": "Unique ID of the protection domain"
        },
        "protectiondomain_name": {
            "type": "string",
            "required": True,
            "description": "Name of the protection domain"
        },
        "members": {
            "type": "array",
            "required": True,
            "description": "List of members of the group",
            "items": {
                "properties": {
                    "member_hostid": {
                        "type": "string",
                        "required": False,
                        "description": "Protection domain member hostname"
                    },
                    "member_hostname": {
                        "type": "string",
                        "required": False,
                        "description": "Unique hostID of the protection domain member"
                    }
                }
            }
        }
    }
}
