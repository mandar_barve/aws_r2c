from managedrecovery.api.doc.django_urls import build_patterns
from managedrecovery.api.resources.protectiondomains import \
    ProtectionDomains, ProtectionDomain
from managedrecovery.api.resources.protectiondomain_member import\
    ProtectionDomainMembers, ProtectionDomainMember
from managedrecovery.api.resources.applications import Groups,\
    ProtectGroups, GroupMembers, Applications, ProtectApps,\
    UnProtectApps, UnProtectGroups
from managedrecovery.api.resources.login import Login
from managedrecovery.api.resources.hosts import Hosts, ProtectHosts,\
    RegisterHosts, DeRegisterHosts, PrepareHosts, UnProtectHosts
from managedrecovery.api.resources.usage.usage import DiskUsage, DedupUsage, SnapUsage,\
    MdlUsage, AggregateDiskUsage, BackupStat, ProtectHost, ProtectionAppSize, ProtectHostCount
from managedrecovery.api.resources.images.mount_image import MountImage
from managedrecovery.api.resources.images.restore_image import RestoreImage

title = "Managed Recovery API"
registered_modules = [
    ProtectionDomainMembers,
    ProtectionDomainMember,
    ProtectionDomains,
    ProtectionDomain,
    Groups,
    GroupMembers,
    ProtectGroups,
    UnProtectGroups,
    Applications,
    ProtectApps,
    UnProtectApps,
    Login,
    Hosts,
    ProtectHosts,
    UnProtectHosts,
    RegisterHosts,
    DeRegisterHosts,
    PrepareHosts,
    DiskUsage,
    DedupUsage,
    SnapUsage,
    MdlUsage,
    AggregateDiskUsage,
    BackupStat,
    MountImage,
    RestoreImage,
    ProtectHost,
    ProtectionAppSize,
    ProtectHostCount
]
version = "1.0"

# TODO: is this important?
baseUri = "http://something.something.sungardas.com"

urlpatterns = build_patterns(registered_modules, version)
