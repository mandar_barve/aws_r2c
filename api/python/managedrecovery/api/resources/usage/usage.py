'''
Created on June 24, 2015

@author: chandra
'''
from managedrecovery.api.doc.annotations import path, description,\
    response, body, queryparameter
from managedrecovery.api.json_schemas.usage_schemas import get_aggregate_diskusage_response,\
get_diskusage_response, get_aggregate_mdlusage_response, get_mdlusage_response,\
get_dedupusage_response, get_snapshotusage_response,get_backupusage_response, get_protect_host_response,\
get_protect_host_count_response,get_total_protect_appsize_response
from managedrecovery.api.json_schemas.examples import get_aggregate_diskusage_response_ex,\
get_diskusage_response_ex, get_aggregate_mdlusage_response_ex, get_mdlusage_response_ex,\
get_dedupusage_response_ex, get_snapshotusage_response_ex,get_backupusage_response_ex,get_protect_host_response_ex,\
get_protect_host_count_response_ex,get_total_protect_appsize_response_ex
from managedrecovery.api.resources.base import Base
from managedrecovery.common.awsr2cError.awsr2c_exception import Awsr2cError
from datetime import datetime
from managedrecovery.api.doc.annotations import passthrough
CURRENT_DATE = datetime.now().strftime('%Y-%m-%d')


@path("usage/aggregateDiskUsage")
class AggregateDiskUsage(Base):

    def __init__(self):
        Base.__init__(self)

    @description("Get details of aggregate(deduppool+snapshotpool+metadata)\
disk usage")
    @response(200, "A copy of a aggregate diskusage object",
              get_aggregate_diskusage_response,
              get_aggregate_diskusage_response_ex)
    @queryparameter("sessionid", "session id of the Sky", True)
    def get(self, request, params, *args, **kwargs):
        try:
            sessionid = request.GET.get("sessionid")
            dp_stat = self.actifio_interface.list_disk_pool_stat(sessionid,
                                                                 CURRENT_DATE)
            return self.get_aggregate_usage(dp_stat, CURRENT_DATE)
        except Exception, err:
            raise Awsr2cError("Found Error during fetch disk usage data:\
            " + str(err))


@path("usage/diskUsage")
class DiskUsage(Base):
    def __init__(self):
        Base.__init__(self)

    @description("Get details of each disk pool usage(dedup,snapshot and meta data\
pool")
    @response(200, "A copy of a diskusage object",
              get_diskusage_response, get_diskusage_response_ex)
    @queryparameter("sessionid", "session id of the Sky", True)
    def get(self, request, params, *args, **kwargs):
        try:
            sessionid = request.GET.get("sessionid")
            startdate = request.GET.get("startdate", CURRENT_DATE)
            poolname = request.GET.get("poolname", "*")
            dp_stat = self.actifio_interface.list_disk_pool_stat(sessionid,
                                                                 startdate,
                                                                 poolname=poolname)
            return map(self.get_usage, dp_stat)
        except Exception, err:
            raise Awsr2cError("Found Error during fetch disk usage data:\
            " + str(err))


@path("usage/dedupUsage")
class DedupUsage(Base):

    def __init__(self):
        Base.__init__(self)

    @description("Get the details of dedup pool usage based on datetime, hostname,\
    and appname)")
    @response(200, "A copy of a dedupusage object",
              get_dedupusage_response,
              get_dedupusage_response_ex)
    @queryparameter("sessionid", "session id of the Sky", True)
    def get(self, request, params, *args, **kwargs):
        sessionid = request.GET.get("sessionid")
        startdate = request.GET.get("startdate", CURRENT_DATE)
        hostname = request.GET.get("hostname", "*")
        appname = request.GET.get("appname", "*")

        try:
            dedup_stat = self.actifio_interface.list_dedup_pool_stat(sessionid,
                                                                     startdate,
                                                                     hostname=hostname,
                                                                     appname=appname)
            return map(self.get_usage, dedup_stat)
        except Exception, err:
            raise Awsr2cError("Found Error during fetch dedup usage data:\
            " + str(err))


@path("usage/snapshotUsage")
class SnapUsage(Base):

    def __init__(self):
        Base.__init__(self)

    @description("Get the details of snapshot pool usage based on datetime, hostname \
    and appname)")
    @response(200, "A copy of a snapshotusage object",
              get_snapshotusage_response,
              get_snapshotusage_response_ex)
    @queryparameter("sessionid", "session id of the Sky", True)
    def get(self, request, params, *args, **kwargs):
        sessionid = request.GET.get("sessionid")
        startdate = request.GET.get("startdate", CURRENT_DATE)
        hostname = request.GET.get("hostname", "*")
        appname = request.GET.get("appname", "*")
        try:
            sp_stat = self.actifio_interface.list_snapshot_pool_stat(sessionid,
                                                                     startdate,
                                                                     hostname=hostname, appname=appname)
            return map(self.get_usage, sp_stat)
        except Exception, err:
            raise Awsr2cError("Found Error during fetch snapshot usage data:\
            " + str(err))


@path("usage/mdlUsage")
class MdlUsage(Base):

    def __init__(self):
        Base.__init__(self)

    @description("Get the details of manage data license  \
    usage based on datetime, hostname and appname")
    @response(200, "A copy of a mdlusage object",
              get_mdlusage_response,
              get_mdlusage_response_ex)
    @queryparameter("sessionid", "session id of the Sky", True)
    def get(self, request, params, *args, **kwargs):
        sessionid = request.GET.get("sessionid")
        startdate = request.GET.get("startdate", CURRENT_DATE)
        hostname = request.GET.get("hostname", "*")
        appname = request.GET.get("appname", "*")
        try:
            mdl_stat = self.actifio_interface.list_mdl_stat(sessionid,
                                                            startdate,
                                                            hostname=hostname,
                                                            appname=appname)
	    res = map(self.get_mdlusage, mdl_stat)
	    res = [each for each in res if each]
            return res
        except Exception, err:
            raise Awsr2cError("Found Error during fetch mdl usage data:\
            " + str(err))


@path("usage/backupStat")
class BackupStat(Base):

    def __init__(self):
        Base.__init__(self)

    @description("Get the backup details")
    @response(200, "A copy of a aggregate mdlusage object",
              get_backupusage_response,
              get_backupusage_response_ex)
    @queryparameter("sessionid", "session id of the Sky", True)
    @passthrough()
    def get(self, request, params, *args, **kwargs):
        sessionid = request.GET.get("sessionid")
        hostname = request.GET.get("hostname", "*")
        appname = request.GET.get("appname", "*")
        backup_stat = self.actifio_interface.backup_stat(sessionid, hostname=hostname, appname=appname)
        return map(self.get_backup_stat, backup_stat)

@path("usage/totalProtectionAppSize")
class ProtectionAppSize(Base):

    def __init__(self):
        Base.__init__(self)

    @description("Get the total protection app size based on datetime")
    @response(200, "A copy of a total protection app size object",
              get_total_protect_appsize_response,
              get_total_protect_appsize_response_ex)
    @queryparameter("sessionid", "session id of the Sky", True)
    def get(self, request, params, *args, **kwargs):
        sessionid = request.GET.get("sessionid")
        startdate = request.GET.get("startdate", CURRENT_DATE)
        try:
            mdl_stat = self.actifio_interface.list_mdl_stat(sessionid,
                                                            startdate,
                                                            appid='0')
            return self.get_protection_app_size(mdl_stat)
        except Exception, err:
            raise Awsr2cError("Found Error during fetch aggregate mdl usage data:\
            " + str(err))

@path("usage/protectHosts")
class ProtectHost(Base):

    def __init__(self):
        Base.__init__(self)

    @description("Get the protect host details")
    @response(200, "A copy of a list of protect host object",
              get_protect_host_response,
              get_protect_host_response_ex)
    @queryparameter("sessionid", "session id of the Sky", True)
    @passthrough()
    def get(self, request, params, *args, **kwargs):
        sessionid = request.GET.get("sessionid")
        host_stat = self.actifio_interface.list_hosts(sessionid, hasagent='true')
        return self.get_host_stat(host_stat)



@path("usage/protectHostCount")
class ProtectHostCount(Base):

    def __init__(self):
        Base.__init__(self)

    @description("Get the number of toal protected vm and app")
    @response(200, "A copy of a toal number of protect host object",
              get_protect_host_count_response,
              get_protect_host_count_response_ex)
    @queryparameter("sessionid", "session id of the Sky", True)
    @passthrough()
    def get(self, request, params, *args, **kwargs):
        sessionid = request.GET.get("sessionid")
        protected_apps = self.actifio_interface.list_sla(sessionid)
        all_apps = self.actifio_interface.list_applications(sessionid)
	hosts = []
	for protected_app in protected_apps:
	    for all_app in all_apps:
		if protected_app.get('appid') == all_app.get('id'):
		   hosts.append(all_app.get('hostid'))
        return [{"total number of protected vm":len(set(hosts)),"total number of protected app":len(protected_apps)}]
