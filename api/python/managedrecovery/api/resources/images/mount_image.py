'''
Created on Aug 7, 2015

@author: sonal.ojha
'''
from managedrecovery.api.resources.base import Base
from managedrecovery.api.doc.annotations import path, description, response,\
    body
from managedrecovery.api.json_schemas.image_schemas import \
    mount_image_response, mount_image
from managedrecovery.api.json_schemas.examples import \
    mount_image_response_ex, mount_image_ex


@path("images/mount/")
class MountImage(Base):

    def __init__(self):
        Base.__init__(self)

    @description("Mount all the synback images of original host on the desired host.\
    The pre-requisiste to this api is <br/>1.) The new_hostname should be \
    installed with all required softwares and registered with Sky. \
    <br/>This api would perform the following steps <br/>1.) Find and mount \
    the latest synback image of the original host.<br/>2.)")
    @response(200, "Image is successfully mounted", mount_image_response,
              mount_image_response_ex)
    @body(mount_image, mount_image_ex)
    def post(self, request, params, *args, **kwargs):
        sessionid = params.get("sessionid")
        old_host = params.get("hostname")
        new_host = params.get("new_hostname")
        image_label = params.get("label")
        protect_new_host = params.get("protect_new_host")

        # retrieve only the syncback images of the failed over host
        images = self.actifio_interface.backup_stat(sessionid,
                                                    hostname=old_host,
                                                    jobclass="syncback")
        self.logger.debug("Syncback images = %s" % images)
        # out of the two synback images retrieve only the image which has a
        # valid originalbackupid
        if len(images) == 0:
            self.logger.debug("No images to mount, skipping ...")
            return {"mount_status": "Failure"}
	
        mount_images, appids = self.find_latest_app_images(images)
        self.logger.debug("latest syncback images = %s" % mount_images)
        self.logger.debug("latest syncback appids = %s" % appids)
        for appname, imageid in mount_images.items():
            self.logger.debug("Mount imageid [%s] for appname [%s]" %
                              (imageid, appname))
            self.actifio_interface.mount_image(sessionid, imageid, new_host,
                                               label=image_label)
        response = {"mount_status": "Success"}
	if not protect_new_host:
            self.failback_host(sessionid, appids)
            response["failback_status"] = "Success"
        return response
