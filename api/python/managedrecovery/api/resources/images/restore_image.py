'''
Created on Aug 7, 2015

@author: sonal.ojha
'''
from managedrecovery.api.resources.base import Base
from managedrecovery.api.doc.annotations import path, description, response,\
    body
from managedrecovery.api.json_schemas.image_schemas import \
    restore_image_response, restore_image
from managedrecovery.api.json_schemas.examples import \
    restore_image_response_ex, restore_image_ex


@path("images/restore/")
class RestoreImage(Base):

    def __init__(self):
        Base.__init__(self)

    @description("Restore all the synback images of the original host.\
    <br/>This api would perform the following steps <br/>1.) Find and restore \
    the latest synback image of the original host. **NOTE** The root filesystem \
    would not be restored by this api <br/>2.)")
    @response(200, "Image is successfully restored", restore_image_response,
              restore_image_response_ex)
    @body(restore_image, restore_image_ex)
    def post(self, request, params, *args, **kwargs):
        sessionid = params.get("sessionid")
        image_host = params.get("hostname")

        # retrieve only the syncback images of the failed over host
        images = self.actifio_interface.backup_stat(sessionid,
                                                    hostname=image_host,
                                                    jobclass="syncback")
        self.logger.debug("Syncback images = %s" % images)
        # out of the two synback images retrieve only the image which has a
        # valid originalbackupid
        restore_images, appids = self.find_latest_app_images(images)
        self.logger.debug("latest syncback images = %s" % restore_images)
        self.logger.debug("latest syncback appids = %s" % appids)

        for appname, imageid in restore_images.items():
            if (appname == "/" or appname == "C:\\"):
                continue
            self.logger.debug("Image[%s] for App[%s] to be restored" %
                              (imageid, appname))
            self.actifio_interface.restore_image(sessionid, imageid)
        response = {"restore_status": "Success"}
        self.failback_host(sessionid, appids)
        response["failback_status"] = "Success"
        return response
