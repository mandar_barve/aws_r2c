'''
Created on May 27, 2015

@author: sonal.ojha
'''
from managedrecovery.api.doc.annotations import path, description,\
    response, body, queryparameter
from managedrecovery.api.resources.base import Base
from managedrecovery.api.json_schemas.protection_domain_schemas import create_protection_domain,\
    create_protection_domain_response, get_protection_domain_response
from managedrecovery.api.json_schemas.examples import create_protection_domain_ex,\
    create_protection_domain_response_ex, get_protection_domain_response_ex
from managedrecovery.common.awsr2cError.awsr2c_exception import Awsr2cError


@path("protectiondomains/")
class ProtectionDomains(Base):

    @description(" Create a protection domain. A protection domain is group of \
    hosts protected by the policy applicable on the group. To create a protected \
    group a host has to be added to the group.")
    @response(200, "A copy of a protection domain object",
              create_protection_domain_response,
              create_protection_domain_response_ex)
    @body(create_protection_domain, create_protection_domain_ex)
    def post(self, request, params, *args, **kwargs):
        '''
        This method would
        1. Creates an actifio group
        2. Retrieve the applications for host_id
        4. Add the applications to the group
        5. Protect the group by applying SLA
        '''
        sessionid = params.get("sessionid")
        if params.get("domain_name") == "":
            raise Awsr2cError("domain_name cannot be blank")
        groupid = self.actifio_interface.create_group(sessionid,
                                                      params.get("domain_name"),
                                                      params.get("description"))
        self.logger.debug("The group %s got created" % groupid)
        response = {"protectiondomain_id": groupid, "members": list()}
        applications = self.actifio_interface.list_applications(sessionid,
                                                                params.get("hostid"))

        for app in applications:
            if app.get("hostid") != params.get("hostid"):
                continue
            memberid = self.actifio_interface.add_group_member(sessionid,
                                                               app.get("id"),
                                                               groupid)
            self.logger.debug("app(%s) added, memberid(%s)" % (app.get("id"),
                                                               memberid))
            self.logger.debug("app_id(%s) added to group, member_id=%s"
                              % (app.get('id'), memberid))
            response["members"].append({"member_hostid": params.get("hostid")})
        self.actifio_interface.protect_group(sessionid,
                                             groupid,
                                             params.get("slp"),
                                             params.get("slt"),
                                             params.get("description"))
        return response


@path("protectiondomains/{protectiondomain_id}/")
class ProtectionDomain(Base):

    @description("Get details of a protection domain")
    @response(200, "A copy of a protection domain object",
              get_protection_domain_response,
              get_protection_domain_response_ex)
    @queryparameter("sessionid", "session id of the Sky", True)
    def get(self, request, params, *args, **kwargs):
        '''
        This method would
        1. List the group details as per protectiondomain_id(i.e groupid)
        2. Retrieve the group members
        4. list hostid as per appid
        5. Get hostname as per the hostid
        6. return the hostname list
        '''
        sessionid = request.GET.get("sessionid")
        group_id = params.get("protectiondomain_id")
        if not sessionid:
            raise Exception("session id query parameter is not valid")
        members = self.actifio_interface.list_group_members(sessionid,
                                                            group_id)
        self.logger.debug("members details =  %s" % members)
        response = list()
        for member in members:
            app_id = member.get("appid")
            self.logger.debug("appid =  %s" % app_id)
            app = self.actifio_interface.list_applications(sessionid,
                                                           appid=app_id)
            # app is a list holding details of one application
            host_id = app[0].get("hostid")
            self.logger.debug("appid=%s belongs to hostid=%s" % (app_id,
                                                                 host_id))
            hosts = self.actifio_interface.list_hosts(sessionid)
            for host in hosts:
                if host.get("id") != host_id:
                    continue
                response.append({"member_hostname": host.get("hostname"),
                                 "member_hostid": host.get("id")})
        return response
