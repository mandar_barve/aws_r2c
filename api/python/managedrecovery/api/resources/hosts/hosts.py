'''
Created on June 30, 2015

@author: meghna.kale
'''
from managedrecovery.api.doc.annotations import path
from managedrecovery.api.resources.base import Base
from managedrecovery.api.doc.annotations import passthrough


@path("hosts/")
class Hosts(Base):

    def __init__(self):
        Base.__init__(self)

    @passthrough()
    def post(self, request, params, *args, **kwargs):
        host_id = self.actifio_interface.add_host(params.get("sessionid"),
                                                  params.get("hostname"),
                                                  params.get("ipaddress"),
                                                  params.get("hosttype"),
                                                  params.get("iscsiname"))
        return host_id

    @passthrough()
    def get(self, request, params, *args, **kwargs):
        sessionid = request.GET.get("sessionid")
        hosts = self.actifio_interface.list_hosts(sessionid)
        return hosts
