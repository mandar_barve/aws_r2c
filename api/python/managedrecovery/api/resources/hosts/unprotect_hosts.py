'''
Created on Jul 21, 2015

@author: sonal.ojha
'''
from managedrecovery.api.doc.annotations import path, description, response,\
    queryparameter
from managedrecovery.api.resources.base import Base
from managedrecovery.common.awsr2cError.awsr2c_exception import Awsr2cError


@path("hosts/unprotect/")
class UnProtectHosts(Base):

    def __init__(self):
        Base.__init__(self)

    @description("Un-protect a protected host")
    @queryparameter("sessionid", "UUID returned on successful login into Sky",
                    True, "string")
    @queryparameter("host_id", "Unique ID of the protected host",
                    True, "string")
    @response(204, "Host is successfully unprotected")
    def delete(self, request, params, *args, **kwargs):
        host_id = request.GET.get('host_id')
        sessionid = request.GET.get('sessionid')

        slas = self.actifio_interface.list_sla(sessionid)
        applications = self.actifio_interface.list_applications(sessionid,
                                                                host_id)
        for app in applications:
            # get app id
            app_id = app["id"]
            for sla in slas:
                if sla['appid'] != app_id:
                    continue
                sla_id = sla.get("id")
                self.logger.debug("sla_id %s" % sla_id)
                if sla_id is None:
                    raise Awsr2cError("Host(%s) cannot be unprotected as app(%s) is not protected"
                                      % (host_id, app_id))
                self.actifio_interface.unprotect_app(sessionid, sla_id)
