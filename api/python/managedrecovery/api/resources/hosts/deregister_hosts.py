'''
Created on Jul 15, 2015

@author: sonal.ojha
'''
from managedrecovery.api.doc.annotations import path, description, response,\
    body
from managedrecovery.api.resources.base import Base
from managedrecovery.api.json_schemas.hosts_schemas import unregister_host_response,\
    unregister_host
from managedrecovery.api.json_schemas.examples import unregister_host_response_ex,\
    unregister_host_ex
from managedrecovery.common.awsr2cError.awsr2c_exception import Awsr2cError


@path("hosts/deregister/")
class DeRegisterHosts(Base):

    def __init__(self):
        Base.__init__(self)

    @description("De-Initialize a host from Sky. This api would perform the following steps\
    <br/>1.) Remove the host from the Sky<br/>2.) Un-install the software \
    installed as a part of prepare host api.")
    @response(200, "Host is successfully de-initialized from Sky",
              unregister_host_response,
              unregister_host_response_ex)
    @body(unregister_host, unregister_host_ex)
    def post(self, request, params, *args, **kwargs):
        hostid = params.get("hostid")
        sessionid = params.get("sessionid")
        if hostid is None:
            hostname = params.get("hostname")
            host = self.actifio_interface.list_hosts(sessionid, hostname)[0]
            hostid = host.get("id")
        if hostid is None:
            raise Awsr2cError("Neither hostname nor hostid parameter provided")
#         ipaddress = params.get("ipaddress")
#         username = params.get("username")
#         password = params.get("password")
#         pkey = params.get("pkey_path")
#         env = params.get("env")
        self.actifio_interface.delete_host(sessionid, hostid)
        self.logger.debug("host(%s) is removed" % hostid)
#         # remove the installed scripts
#         interface = self.get_command_interface(ipaddress, username, password,
#                                                pkey, env)
#         if not interface.cleanup():
#             return {"status": False}
        return {"status": True}
