'''
Created on Jul 15, 2015

@author: sonal.ojha
'''
from managedrecovery.api.doc.annotations import path, description, response,\
    body
from managedrecovery.api.resources.base import Base, HOSTTYPE_DEFAULT,\
    HOSTTYPES
from managedrecovery.api.json_schemas.hosts_schemas import register_host_response,\
    register_host
from managedrecovery.api.json_schemas.examples import register_host_response_ex,\
    register_host_ex
from managedrecovery.common.awsr2cError.awsr2c_exception import Awsr2cError


@path("hosts/register/")
class RegisterHosts(Base):

    def __init__(self):
        Base.__init__(self)

    @description("Initialize a host for Sky. This api will perform the \
    following steps <br/>1.) Add the host to the Sky <br/>2.) Run ISCSI \
    test on the Sky added host (optional)<br/>3.) Get all host applications \
    discovered by Sky")
    @response(200, "Host is registered successfully", register_host_response,
              register_host_response_ex)
    @body(register_host, register_host_ex)
    def post(self, request, params, *args, **kwargs):
        ipaddress = params.get("ipaddress")
        hostname = params.get("hostname")
        sessionid = params.get("sessionid")
        iqnname = params.get("iqnname")
        hosttype = HOSTTYPE_DEFAULT
        if params.get("hosttype"):
            for t in HOSTTYPES:
                if params.get('hosttype') == t[0]:
                    hosttype = t[0]
                    break
        host_id = self.actifio_interface.add_host(sessionid, hostname,
                                                  ipaddress, hosttype,
                                                  iqnname)
        response = {"hostid": host_id}
        try:
            self.actifio_interface.test_iscsi(sessionid, hostname)
            response["test_iscsi_status"] = "success"
        except Awsr2cError, err:
            if err.msg.__contains__("ISCSI test failed"):
                # delete the added host
                try:
                    self.logger.debug("deleting host[%s], test_iscsi failed"
                                      % host_id)
                    self.actifio_interface.delete_host(sessionid, host_id)
                    response["test_iscsi_status"] = "failed"
                    response["hostid"] = None
                    return response
                except Awsr2cError, err:
                    self.logger.debug("added host[%s] cannot be delete" %
                                      host_id)
                    response["test_iscsi_status"] = "failed"
                    response["hostid"] = None
                    return response
        self.actifio_interface.app_discover(sessionid, hostname)
        return response
