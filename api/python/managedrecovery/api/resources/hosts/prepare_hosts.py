'''
Created on Jul 15, 2015

@author: sonal.ojha
'''
from managedrecovery.api.resources.base import Base
from managedrecovery.api.doc.annotations import path, description, response,\
    body
from managedrecovery.common.awsr2cError.awsr2c_exception import Awsr2cError
from managedrecovery.api.json_schemas.hosts_schemas import prepare_host,\
    prepare_host_response
from managedrecovery.api.json_schemas.examples import prepare_win_host_ex,\
    prepare_lin_host_ex, prepare_host_response_ex


@path("hosts/prepare/")
class PrepareHosts(Base):

    def __init__(self):
        Base.__init__(self)

    @description("Prepare the host for Sky by installing necessary softwares")
    @response(200, "Host preparation is complete",
              prepare_host_response,
              prepare_host_response_ex)
    @body(prepare_host, prepare_lin_host_ex)
    def post(self, request, params, *args, **kwargs):
        env = params.get("env")
        ipaddress = params.get("ipaddress")
        username = params.get("username")
        password = params.get("password")
        pkey = params.get("pkey_path")
        sky_ip = params.get("sky_ip")
        interface = self.get_command_interface(ipaddress, username,
                                               password, pkey, env)
        output = interface.initialize_env(sky_ip)
        iqnname = interface.get_iqnname(output)
        if iqnname is None or iqnname == "":
            raise Awsr2cError("prepare host failed, could not retrieve ISCSI Unique name")
        response = {"iqnname": iqnname}
        return response
