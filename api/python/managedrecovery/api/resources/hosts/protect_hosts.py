'''
Created on Jul 15, 2015

@author: sonal.ojha
'''
from managedrecovery.api.doc.annotations import path, description, response,\
    body
from managedrecovery.api.resources.base import Base
from managedrecovery.api.json_schemas.hosts_schemas import protected_host
from managedrecovery.api.json_schemas.examples import protect_host_ex


@path("hosts/protect/")
class ProtectHosts(Base):

    def __init__(self):
        Base.__init__(self)

    @description("Protect a host. This api will protect all the host applications")
    @response(200, "Protect a host", "Host is successfully protected",
              "Host is successfully protected")
    @body(protected_host, protect_host_ex)
    def post(self, request, params, *args, **kwargs):
        '''
        # 1) list applications for the host
        # 2) protect all the host applications
        '''
        # Call the Linux command interface
        sessionid = params.get("sessionid")
        host_id = params.get("hostid")

        applications = self.actifio_interface.list_applications(sessionid,
                                                                host_id)
        for app in applications:
            app_id = app.get("id")
            description = "samplesla"
            self.actifio_interface.protect_app(sessionid, app_id,
                                               params.get("slp"),
                                               params.get("slt"), description)
            return "Host is successfully protected"
