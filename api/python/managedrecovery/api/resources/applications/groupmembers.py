'''
Created on June 26, 2015

@author: meghna.kale
'''
from managedrecovery.api.doc.annotations import path
from managedrecovery.api.resources.base import Base
from managedrecovery.api.doc.annotations import passthrough


@path("applications/groups/members/")
class GroupMembers(Base):

    def __init__(self):
        Base.__init__(self)

    @passthrough()
    def post(self, request, params, *args, **kwargs):
        response = self.actifio_interface.add_group_member(params.get("sessionid"),
                                                           params.get("app_id"),
                                                           params.get("group_id"))
        return response

    @passthrough()
    def get(self, request, params, *args, **kwargs):
        sessionid = request.GET.get("sessionid")
        response = self.actifio_interface.list_group_members(sessionid)
        return response

    @passthrough()
    def delete(self, request, params, *args, **kwargs):
        sessionid = request.GET.get("sessionid")
        group_member_id = request.GET.get("group_member_id")
        response = self.actifio_interface.delete_group_member(sessionid,
                                                              group_member_id)
        return response
