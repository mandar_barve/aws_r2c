'''
Created on Jul 21, 2015

@author: sonal.ojha
'''
from managedrecovery.api.doc.annotations import path, description,\
    queryparameter, response
from managedrecovery.api.resources.base import Base


@path("applications/groups/unprotect/")
class UnProtectGroups(Base):

    def __init__(self):
        Base.__init__(self)

    @description("Un-Protect a protected Sky group")
    @queryparameter("sessionid", "UUID returned on successful login into Sky",
                    True, "string")
    @queryparameter("group_id", "Unique ID of the protected Sky group",
                    True, "string")
    @response(204, "The Sky group is successfully unprotected")
    def delete(self, request, params, *args, **kwargs):
        sessionid = request.GET.get("sessionid")
        group_id = request.GET.get("group_id")
        self.actifio_interface.unprotect_group(sessionid, group_id)
