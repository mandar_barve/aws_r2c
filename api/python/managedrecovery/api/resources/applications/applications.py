'''
Created on June 24, 2015

@author: meghna.kale
'''
from managedrecovery.api.doc.annotations import path
from managedrecovery.api.resources.base import Base
from managedrecovery.api.doc.annotations import passthrough


@path("hosts/applications/")
class Applications(Base):

    def __init__(self):
        Base.__init__(self)

    @passthrough()
    def get(self, request, params, *args, **kwargs):
        sessionid = request.GET.get("sessionid")
        hostid = request.GET.get("hostid")
        response = self.actifio_interface.list_applications(sessionid,
                                                            hostid)
        return response
