'''
Created on Jul 15, 2015

@author: sonal.ojha
'''
from managedrecovery.api.resources.base import Base
from managedrecovery.api.doc.annotations import path, passthrough


@path("hosts/applications/protect/")
class ProtectApps(Base):

    def __init__(self):
        Base.__init__(self)

    @passthrough()
    def post(self, request, params, *args, **kwargs):
        response = self.actifio_interface.protect_app(params.get("sessionid"),
                                                      params.get("app_id"),
                                                      params.get("slp"),
                                                      params.get("slt"),
                                                      params.get("description"))
        return response
