'''
Created on June 26, 2015

@author: meghna.kale
'''
from managedrecovery.api.doc.annotations import path, description, queryparameter,\
    response
from managedrecovery.api.resources.base import Base
from managedrecovery.api.doc.annotations import passthrough


@path("applications/groups/")
class Groups(Base):

    def __init__(self):
        Base.__init__(self)

    @passthrough()
    def post(self, request, params, *args, **kwargs):
        response = self.actifio_interface.create_group(params.get("sessionid"),
                                                       params.get("group_name"),
                                                       params.get("description"))
        return response

    @passthrough()
    def get(self, request, params, *args, **kwargs):
        sessionid = request.GET.get("sessionid")
        response = self.actifio_interface.list_groups(sessionid)
        return response

    @description("Delete a sky group")
    @queryparameter("sessionid", "UUID returned on successful login into Sky",
                    True, "string")
    @queryparameter("group_id", "Unique ID of the Sky Group",
                    True, "string")
    @response(204, "This api doesnt return any content")
    def delete(self, request, params, *args, **kwargs):
        sessionid = request.GET.get("sessionid")
        group_id = request.GET.get("group_id")
        self.actifio_interface.delete_group(sessionid, group_id)
