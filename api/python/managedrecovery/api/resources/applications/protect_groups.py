'''
Created on Jul 15, 2015

@author: sonal.ojha
'''
from managedrecovery.api.doc.annotations import path, passthrough
from managedrecovery.api.resources.base import Base


@path("applications/groups/protect/")
class ProtectGroups(Base):

    def __init__(self):
        Base.__init__(self)

    @passthrough()
    def post(self, request, params, *args, **kwargs):
        response = self.actifio_interface.protect_group(params.get("sessionid"),
                                                        params.get("group_id"),
                                                        params.get("slp"),
                                                        params.get("slt"),
                                                        params.get("description"))
        return response
