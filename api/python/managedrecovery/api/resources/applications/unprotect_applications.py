'''
Created on Jul 21, 2015

@author: sonal.ojha
'''
from managedrecovery.api.resources.base import Base
from managedrecovery.api.doc.annotations import path, description,\
    queryparameter, response


@path("hosts/applications/unprotect/")
class UnProtectApps(Base):

    def __init__(self):
        Base.__init__(self)

    @description("Un-Protect an already protected application")
    @queryparameter("sessionid", "UUID returned on successful login into Sky",
                    True, "string")
    @queryparameter("app_id", "Unique ID of the protected application",
                    True, "string")
    @response(204, "The application is successfully unprotected")
    def delete(self, request, params, *args, **kwargs):
        app_id = request.GET.get("app_id")
        sessionid = request.GET.get("sessionid")
        sla_id = ""
        slas = self.actifio_interface.list_sla(sessionid)
        for sla in slas:
            if sla['appid'] != app_id:
                continue
            sla_id = sla['id']
            self.actifio_interface.unprotect_app(sessionid, sla_id)
