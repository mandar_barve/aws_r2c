'''
Created on May 27, 2015

@author: chandra
'''
from managedrecovery.api.doc.annotations import path, description,\
    response, body, queryparameter
from managedrecovery.api.resources.base import Base
from managedrecovery.api.json_schemas.protection_domain_member_schemas import \
    add_protection_domain_mem_response, add_protection_domain_mem,\
    delete_protection_domain_mem_response
from managedrecovery.api.json_schemas.examples import add_protection_domain_mem_response_ex,\
    add_protection_domain_mem_ex, delete_protection_domain_mem_response_ex
from managedrecovery.common.awsr2cError.awsr2c_exception import Awsr2cError


@path("protectiondomains/members/")
class ProtectionDomainMembers(Base):

    @description("Add a host to protection domain.")
    @response(200, "A copy of a protection domain object",
              add_protection_domain_mem_response,
              add_protection_domain_mem_response_ex)
    @body(add_protection_domain_mem, add_protection_domain_mem_ex)
    def post(self, request, params, *args, **kwargs):
        '''
        This method would
        1. find groupid based on group name
        2. Retrieve the applications for host_id
        3. Add the applications to the group
        '''
        sessionid = params.get("sessionid")
        grp_name = params.get("domain_name")
        hostid = params.get("hostid")
        if grp_name == "":
            raise Awsr2cError("domain_name cannot be blank")
        group = self.actifio_interface.list_groups(sessionid,
                                                   group_name=grp_name)
        if not group:
            self.logger.error("Protection Domain(%s) doesn't exists" % grp_name)
            raise Awsr2cError("Protection Domain(%s) doesn't exists" % grp_name)

        group_id = group[0].get('id')
        response = {"protectiondomain_name": grp_name,
                    "protectiondomain_id": group_id, "members": []}
        apps = self.actifio_interface.list_applications(sessionid,
                                                        hostid=hostid)
        for app in apps:
            memberid = self.actifio_interface.add_group_member(sessionid,
                                                               app.get("id"),
                                                               group_id)
            self.logger.debug("appid(%s) added to groupid(%s), memberid=%s"
                              % (app.get('id'), group_id, memberid))
            response['members'].append({"member_hostid": hostid,
                                        "member_hostname": ""})
        return response


@path("protectiondomains/members/{memberid}/")
class ProtectionDomainMember(Base):

    @description("Delete a host member from the protection domain. Deleting a host member will unprotect the host.")
    @response(200, "A copy of a protection domain object",
              delete_protection_domain_mem_response,
              delete_protection_domain_mem_response_ex)
    @queryparameter("sessionid", "UUID returned on successful login into Sky",
                    True)
    @queryparameter("domain_name", "Name of the protection domain",
                    True)
    def delete(self, request, params, *args, **kwargs):
        '''
        This method would
        1. Retrieve the applications for host_id
        2. Retrieve all group members id
        3. Delete group member baded on member id
        3. Retrieve slaid based on appid
        5. Unprotect app based on slaid
        '''
        sessionid = request.GET.get("sessionid")
        grp_name = request.GET.get("domain_name")
        if not sessionid or not grp_name:
            raise Awsr2cError("Invalid url query parameters")
        hostid = params.get("memberid")
        group = self.actifio_interface.list_groups(sessionid,
                                                   group_name=grp_name)
        if not group:
            raise Awsr2cError("Protection Domain(%s) doesn't exists" % grp_name)

        response = {"protectiondomain_name": grp_name,
                    "protectiondomain_id": group[0].get('id'),
                    "members": []}
        apps = self.actifio_interface.list_applications(sessionid, hostid)
        for app in apps:
            appid = app.get("id")
            member = self.actifio_interface.list_group_members(sessionid,
                                                               app_id=appid)
            if not member:
                continue
            memid = member[0].get('id')
            self.actifio_interface.delete_group_member(sessionid, memid)
            self.logger.debug("memberid(%s) successfully deleted" % (memid))
            sla = self.actifio_interface.list_sla(sessionid,
                                                  app_id=app.get("id"))
            if not sla:
                continue
            self.actifio_interface.unprotect_app(sessionid,
                                                 sla[0].get('id'))
            self.logger.debug("appid (%s) unprotected" % app.get("id"))
            response['members'].append({"member_hostname": "",
                                        "member_hostid": hostid})
            groupid = group[0].get("id")
            member = self.actifio_interface.list_group_members(sessionid,
                                                               group_id=groupid)
            if not member:
                self.actifio_interface.delete_group(sessionid, groupid)
                self.logger.debug("groupid(%s) successfully deleted" % groupid)
        return response
