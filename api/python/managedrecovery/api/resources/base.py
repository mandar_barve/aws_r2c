'''
Created on May 27, 2015

@author: sonal.ojha
'''
import httplib
from managedrecovery.api.doc.get_annotation import AnnotatedClass
from jsonschema.validators import validate
from django.http import HttpResponse
import json
from django.http import HttpResponseNotFound, HttpResponseBadRequest,\
    HttpResponseServerError, HttpResponseNotAllowed
from managedrecovery.api.interfaces.actifio import ActifioInterface
from django.conf import settings
import logging
import logging.config
from managedrecovery.common.logger.awsr2c_logging import Awsr2clogging
from managedrecovery.api.interfaces.command import CommandInterface
from managedrecovery.common.awsr2cError import Awsr2cError

HOSTTYPES = (
    ('none', 'none'),
    ('generic', 'generic'),
    ('HP-UX', 'HP-UX'),
    ('TPGS', 'TPGS'),
    ('Open VMS', 'Open VMS'),
    ('vCenter', 'vCenter'),
    ('IBM HMC', 'IBM HMC'),
    ('Hyper-V Server', 'Hyper-V Server'),
    ('SCVMM', 'SCVMM'),
)

HOSTTYPE_NONE = HOSTTYPES[0][1]
HOSTTYPE_GENERIC = HOSTTYPES[1][1]
HOSTTYPE_HP = HOSTTYPES[2][1]
HOSTTYPE_TPGS = HOSTTYPES[3][1]
HOSTTYPE_OPENVM = HOSTTYPES[4][1]
HOSTTYPE_VCENTER = HOSTTYPES[5][1]
HOSTTYPE_IBM = HOSTTYPES[6][1]
HOSTTYPE_HYPERV = HOSTTYPES[7][1]
HOSTTYPE_SCVMM = HOSTTYPES[8][1]

HOSTTYPE_DEFAULT = HOSTTYPE_GENERIC


class Base(AnnotatedClass):
    '''
    This is the base class for all the REST api resources
    '''

    def __init__(self):
        self.method_fields = {}
        logconfig_path = settings.LOGGER_CONFIG_PATH
        logging.config.fileConfig(logconfig_path)
        self.logger = Awsr2clogging.getLogger("awsr2c.actifio", Awsr2clogging.COMPONENT_API)

    def __call__(self, request, *args, **kwargs):
        self.request = request
        req_method = request.method
        method_name = req_method.lower()
        try:
            # pull the method from the dictionary
            method = self.__class__.__dict__[method_name]
        except Exception, ex:
            return self._create_error_response("The method %s is not supported"
                                               % (method_name,
                                                  httplib.METHOD_NOT_ALLOWED)
                                               )
        # Get the JSON, errors here are the caller's fault!
        try:
            json_params = self.get_json(request, *args, **kwargs)
            self.logger.debug("params = %s" % json_params)
            schema = self.get_schema(method)
            validate(json_params, schema)
            if method_name in ['get', 'delete']:
                ip = self.request.GET.get('sky_ip')
            else:
                ip = json_params.get('sky_ip')
            if not ip:
                return self._create_error_response("Please provide sky hostname(or IP)")
            self.actifio_interface = ActifioInterface(ip)

        except Exception, ex:
            self.logger.error("sending bad request error response = %s"
                              % str(ex))
            # Blame everything on the caller:
            return self._create_error_response(ex, httplib.BAD_REQUEST)

        try:
            # call the method
            ret = method(self, request, json_params, *args, **kwargs)

            if isinstance(ret, HttpResponse):
                return ret

            self.logger.debug("sending success response = %s" % str(ret))
            return self._create_success_response(ret)

        # return 500 error code:
        except Exception, ex:
            self.logger.error("sending error response = %s" % str(ex))
            return self._create_error_response(ex,
                                               httplib.INTERNAL_SERVER_ERROR)

    def get_json(self, request, *args, **kwargs):
        """
        This is a generic method used to generate and validate the JSON parameters
        It performs the following functionality:
            1. Set the params for the request.
            2. Call appropriate handler for different HTTP Request methods.
        """
        json_params = {}

        for key in kwargs.keys():
            json_params[key] = kwargs[key]

        req_body_params = {}
        if hasattr(request, 'body'):
            self.logger.debug("request.body [%s]" % request.body)
            if (None != request.body) and (len(request.body) > 0):
                req_body_params = json.loads(request.body)
        else:
            if (None != request.raw_post_data) and (len(request.raw_post_data) > 0):
                req_body_params = json.loads(request.raw_post_data)
        if isinstance(req_body_params, dict):
            for key in req_body_params.keys():
                json_params[key] = req_body_params.get(key)
        elif isinstance(req_body_params, list):
            json_params = req_body_params

        return json_params

    def _create_error_response(self, body="", error_code=None):
        """
        This method creates the JSON error response with caller
        defined response code.
        """
        if error_code == httplib.NOT_FOUND:
            return HttpResponseNotFound(body)
        elif error_code == httplib.INTERNAL_SERVER_ERROR:
            return HttpResponseServerError(body)
        elif error_code == httplib.BAD_REQUEST:
            return HttpResponseBadRequest(body)
        elif error_code == httplib.METHOD_NOT_ALLOWED:
            return HttpResponseNotAllowed(body)
        else:
            return HttpResponseServerError(body)

    def _create_success_response(self, body):
        """
        This method creates the JSON success response.
        """
        response = HttpResponse()
        response.content = json.dumps(body)
        response.status_code = httplib.OK
        return response

    def get_command_interface(self, public_ip, username, password, pkey, env):
        interface = None
        if env.lower() == 'linux':
            interface = CommandInterface(public_ip, "ec2-user", pkey=pkey,
                                         env=env)
        elif env.lower() == 'windows':
            interface = CommandInterface(public_ip, username, passwd=password,
                                         env=env)
        else:
            raise Awsr2cError("OS type not supported")
        return interface

    def convert_byte_to_gb(self, val):
        return "%.4f" % (abs(int(val)/(1024.0*1024.0*1024.0)))

    def get_usage(self, each):
        if each.get('appsize'):each['appsize'] = str(self.convert_byte_to_gb(each['appsize'])) + " GB"
        if each.get('totalused'):each['totalused'] = str(self.convert_byte_to_gb(each['totalused'])) + " GB"
        if each.get('totalstaging'):each['totalstaging'] = str(self.convert_byte_to_gb(each['totalstaging'])) + " GB"
        if each.get('dedupusage'): each['dedupusage'] = str(self.convert_byte_to_gb(each['dedupusage']))+ " GB"
        if each.get('newsize'):each['newsize'] = self.convert_byte_to_gb(each['newsize'])+ " GB"
        if each.get('totalappsize'):each['totalappsize'] = self.convert_byte_to_gb(each['totalappsize'])+ " GB"
        if each.get('compresssize'):each['compresssize'] = self.convert_byte_to_gb(each['compresssize'])+ " GB"
        if each.get('dedupsize'):each['dedupsize'] = self.convert_byte_to_gb(each['dedupsize'])+ " GB"
        if each.get('used'):each['used'] = str(self.convert_byte_to_gb(each['used']))
        if each.get('used') and each.get('capacity'):
            each['capacity'] = self.convert_byte_to_gb(each['capacity'])
            each['free'] = "%.2f" % (float(each['capacity']) - float(each['used'])) + " GB"
            perct = "%.2f" % (float(each['used'])*100/float(each['capacity']))
            each['used(%)'] = str(perct)+"%"
            each['used'] = each['used'] + " GB"
            each['capacity'] =  each['capacity'] + " GB"
        if each.get('id'): each.pop('id')
        if each.get('sourcecluster'): each.pop('sourcecluster')
        if each.get('pooltype'): each.pop('pooltype')
        return each

    def get_aggregate_usage(self, usagedata, st):
	agrgt_usage = {"used":0,"capacity":0,"free":0}
        for each in usagedata:
	    agrgt_usage['used'] = int(agrgt_usage['used'])+ int(each['used'])
	    agrgt_usage['capacity'] = int(agrgt_usage['capacity'])+ int(each['capacity'])
            agrgt_usage['free'] = int(agrgt_usage['capacity']) - int(each['used'])

        agrgt_usage['used(%)'] =  "%.2f" % (float(agrgt_usage['used'])*100/float(agrgt_usage['capacity'])) + "%"
        agrgt_usage['used'] = str(self.convert_byte_to_gb(agrgt_usage['used']))+" GB"
	agrgt_usage['capacity'] = str(self.convert_byte_to_gb(agrgt_usage['capacity']))+" GB"
	agrgt_usage['free']= str(self.convert_byte_to_gb(agrgt_usage['free']))+" GB"
	agrgt_usage['stattime']= st
	agrgt_usage['poolname'] = 'aggregate storage pool stat'
	return [agrgt_usage]
        
	


    def get_mdlusage(self, each):
        if each.get('appid') == '0': return {}
        if each.get('manageddata'):each['manageddata'] = str(self.convert_byte_to_gb(each['manageddata']))+ " GB"
        if each.get('appsize'): each['appsize'] = str(self.convert_byte_to_gb(each['appsize'])) + " GB"
        if each.get('allocated'): each['allocated'] = str(self.convert_byte_to_gb(each['allocated']))+ " GB"
        if each.get('appreserved'): each.pop('appreserved')
        if each.get('id'): each.pop('id')
        if each.get('devsize'): each.pop('devsize')
        if each.get('appname') == '': each.pop('appname')
        if each.get('appid') == '0': each.pop('appid')
        if each.get('hostname') == '': each.pop('hostname')
        if each.get('sourcecluster'): each.pop('sourcecluster')
        return each

    def get_protection_app_size(self, lst):
	res = []
	for each in lst:
	   fltdata = {"total protection app size": str(self.convert_byte_to_gb(each.get("manageddata"))) + " GB",\
           "stattime":each.get("stattime")}
	   res.append(fltdata)
	return res

    def get_host_stat(self, lst):
	res = []
	for each in lst:
	   fltdata = {"hostname":each.get("hostname","-"),"ostype":each.get("ostype","-"),\
           "ipaddress":each.get("ipaddress","-"),"installeddate":each.get("installeddate","-"),\
           "modifydate":each.get("modifydate","-")}
	   res.append(fltdata)
	return res

    def get_backup_stat(self, each):
        if each.get('virtualsize'):each['virtualsize'] = str(self.convert_byte_to_gb(each['virtualsize']))+ " GB"
        if each.get('originatinguds'): each.pop('originatinguds')
        if each.get('id'): each.pop('id')
        if each.get('username'): each.pop('username')
        if each.get('label'): each.pop('label')
        if each.get('uniquehostname'): each.pop('uniquehostname')
        if each.get('originalbackupid'): each.pop('originalbackupid')
        if each.get('flags'): each.pop('flags')
        if each.get('transport'): each.pop('transport')
        if each.get('consistency-mode'): each.pop('consistency-mode')
        if each.get('sltname'): each.pop('sltname')
        if each.get('backuplock'): each.pop('backuplock')
        if each.get('policyname'): each.pop('policyname')
        if each.get('slpname'): each.pop('slpname')
        if each.get('label') or each.get('label') == '': each.pop('label')
        if each.get('componenttype'): each.pop('componenttype')
        if each.get('apptype'): each.pop('apptype')
        if each.get('sourceimage') or each.get('sourceimage') == '': each.pop('sourceimage')
        if each.get('sourceuds'): each.pop('sourceuds')
        if each.get('prepdate') or each.get('prepdate') == '': each.pop('prepdate')
        return each

    def failback_host(self, sessionid, appids):
        # find the apps for the host and failback for each app
        appids = set(appids)	
        for appid in appids:
            self.logger.debug("App[%s] is being failedback" % appid)
            self.actifio_interface.failback(sessionid, appid)

    def find_latest_app_images(self, images):
        latest_images = dict()
	latest_appids = []
        for image_i in images:
            imageid, appid = None, None
            if image_i.get("originalbackupid") == "0":
                continue
            imageid = image_i.get("id")
	    appid =  image_i.get("appid")
            for image_j in images:
                if (image_j.get("originalbackupid") == "0" or
                   image_j.get("appname") != image_i.get("appname")):
                    continue
                if image_j.get("id") > image_i.get("id"):
                    imageid = image_j.get("id")
		    appid = image_j.get('appid')
            latest_images[image_i.get("appname")] = imageid
            latest_appids.append(appid)
        return latest_images, latest_appids
