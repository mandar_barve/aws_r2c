'''
Created on June 24, 2015

@author: meghna.kale
'''
from managedrecovery.api.doc.annotations import path
from managedrecovery.api.resources.base import Base
from managedrecovery.api.doc.annotations import passthrough


@path("login/")
class Login(Base):

    def __init__(self):
        Base.__init__(self)

    @passthrough()
    def get(self, request, params, *args, **kwargs):
        username = request.GET.get("username")
        password = request.GET.get("password")
        vendorkey = request.GET.get("vendorkey")
        if vendorkey is None:
            vendorkey = ""
        sessionid = self.actifio_interface.login(username, password,
                                                 vendorkey)
        return sessionid
