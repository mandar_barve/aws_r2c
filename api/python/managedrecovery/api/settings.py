"""
Django settings for apollo.api_manager project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '8tuf5w=ky=0=r!)w2+wc05e=r2@hdg4*5xag=24*(2@u=t1@c5'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
)

ROOT_URLCONF = 'managedrecovery.api.urls'

WSGI_APPLICATION = "managedrecovery.api.wsgi.application"

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'

# swf
AWS_KEY = ''
AWS_SECRET = ''
SWF_DOMAIN_NAME = ''
SWF_WORKFLOW_VERSION = '1.0'
LOGGER_CONFIG_PATH = "/etc/awsr2c/logging.cfg"

REMOTE_DIR = "/tmp/managedrecovery"
LOCAL_DIR = "/etc/awsr2c"
RESTART_ISCI = "Start-Service MSiSCSI"
# get_iqnname = "(Get-InitiatorPort).NodeAddress"
# this would work for both windows 2012 and 2008
GET_IQNNAME = '$out=\\"ListInitiators\\" | iscsicli; $out'
INIT_SCRIPT = "linux_connector.sh"
CLEANUP_SCRIPT = "cleanup.sh"
PKEY = "/tmp/managedrecovery/MeghnaNCalifornia.pem"
# Windows related cmd
WINDOWS_BASE_PATH = "C:\\Sungard"
CREATE_DIR_CMD = "cmd /c mkdir " + WINDOWS_BASE_PATH
DOWNLOAD_LOCATION = WINDOWS_BASE_PATH + "\\connector-Win32-latestversion.exe"
DOWNLOAD_LINK = "http://%s/connector-Win32-latestversion.exe"
DOWNLOAD_BASE_CMD = "(New-Object System.Net.WebClient).DownloadFile"
INSTALL_CONNECTOR_CMD = WINDOWS_BASE_PATH + "\\connector-Win32-latestversion.exe /VERYSILENT"
REMOVE_DIR_CMD = "cmd /c rmdir " + WINDOWS_BASE_PATH
WHERE_CLAUSE = 'where "Name="UDSAgent"" get PathName'
STOP_ISCI = "Stop-Service MSiSCSI"
STOP_UDSAGENT_CMD = "Stop-Service UDSAgent"
