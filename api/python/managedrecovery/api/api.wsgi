#!/usr/local/bin/python
import os, sys

path = '/usr/lib/python2.7/site-packages/'
if path not in sys.path:
    sys.path.append(path)

os.environ['DJANGO_SETTINGS_MODULE'] = 'managedrecovery.api.settings'

sys.stdout = sys.stderr

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
