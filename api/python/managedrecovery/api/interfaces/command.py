from managedrecovery.api.interfaces.util.SFTPClient import SFTPClient
import os
import subprocess
from managedrecovery.common.logger.awsr2c_logging import Awsr2clogging
from managedrecovery.common.awsr2cError.awsr2c_exception import Awsr2cError
import logging.config
from django.conf import settings


class CommandInterface():

    def __init__(self, host, username, passwd=None, pkey=None, port=22,
                 env='Linux'):
        logging.config.fileConfig(settings.LOGGER_CONFIG_PATH)
        if env == 'Linux':
            self.interface = LinuxInterface(host, username, pkey)
        else:
            self.interface = WindowsInterface(host, username, passwd)

    def run(self, *args):
        return self.interface.run(*args)

    def get_iqnname(self, output):
        return self.interface.get_iqnname(output)

    def cleanup(self, *args):
        return self.interface.cleanup(*args)

    def initialize_env(self, sky_ip):
        return self.interface.initialize_env(sky_ip)


class LinuxInterface():

    def __init__(self, host, user, pkey):
        self.host = host
        self.logger = Awsr2clogging.getLogger("awsr2c.linux_command_interface",
                                              Awsr2clogging.COMPONENT_LINUX_COMMAND_INTERFACE)
        self.client = SFTPClient(host, user, pkey, self.logger)

    def run(self, script_path, *args):
        self.logger.debug("execute script(%s) on host(%s) " % (script_path,
                                                               self.host))
        output = self.client.run_script(script_path, *args)
        # TODO :: below line shows error, fix it
#             self.logger.debug("output(%s) on host(%s) " % (output, self.host))
        return output

    def initialize_env(self, sky_ip):
        filename = settings.INIT_SCRIPT
        try:
            local_path = os.path.join(settings.LOCAL_DIR, filename)
            remote_path = settings.REMOTE_DIR + '/' + os.path.basename(filename)
            self.logger.debug("create dir(%s) on host(%s)" % (remote_path,
                                                              self.host))
            self.client.create_dir(settings.REMOTE_DIR)
            self.logger.debug("send file(%s) to host(%s) on path(%s)" % (local_path,
                                                                         self.host,
                                                                         remote_path))
            self.client.send_file(local_path, remote_path)
            self.logger.debug("execute script(%s) on host(%s) " % (remote_path,
                                                                   self.host))
            return self.run(remote_path, sky_ip)

        except Exception, exp:
            self.logger.error("Error(%s): Linux initialize_env" % exp)
            raise Awsr2cError(exp)

    def get_iqnname(self, output):
        if output and output.__contains__("InitiatorName"):
            initiator_name = output.split("InitiatorName=")[1]
            self.logger.debug("Linux initiator name=[%s]" % initiator_name)
            return initiator_name

    def cleanup(self):
        filename = settings.CLEANUP_SCRIPT
        try:
            local_path = os.path.join(settings.LOCAL_DIR, filename)
            remote_path = settings.REMOTE_DIR + '/' + os.path.basename(filename)
            self.client.send_file(local_path, remote_path)
            self.logger.debug("sent file(%s) to host(%s) on path(%s)" %
                              (local_path, self.host, remote_path))
            output = self.client.run_script(remote_path)
            self.logger.debug("Linux cleanup runscript output=[%s]" %
                              str(output))
            self.logger.debug("Linux cleanup remove_dir_cmd=[%s]" %
                              settings.REMOTE_DIR)
            self.client.remove_dir(settings.REMOTE_DIR)
            return output  # this returns string
        except Exception, exp:
            self.logger.error("Error(%s): Linux cleanup" % exp)
            raise Awsr2cError(exp)


class WindowsInterface():

    def __init__(self, host, user, password):
        self.host = host
        self.logger = Awsr2clogging.getLogger("awsr2c.windows_command_interface",
                                              Awsr2clogging.COMPONENT_WINDOWS_COMMAND_INTERFACE)
        self.winexe_cmd = 'winexe -U HOME/' + user + '%"' + password + '" //' + host
        self.wmic_cmd = "%s 'wmic service" % self.winexe_cmd
        self.powershell_cmd = "%s 'powershell.exe -Command" % self.winexe_cmd

    def run(self, prompt_type, *commands):
        try:
            cmds = ""
            for command in commands:
                cmds += "%s;" % (command)
            if cmds.endswith(";"):
                cmds = cmds[:-1]
            if prompt_type == "wmic":
                exe_cmd = "%s %s'" % (self.wmic_cmd,
                                      cmds)
            else:
                # trying to run the command on powershell
                exe_cmd = "%s %s'" % (self.powershell_cmd,
                                      cmds)
            self.logger.debug("executing command [%s]" % exe_cmd)
            p = subprocess.Popen(exe_cmd, shell=True, stdout=subprocess.PIPE)
            p.wait()
            stdout = p.stdout.readlines()  # this returns list
            self.logger.debug("response from command [%s]" % str(stdout))
            return "".join(stdout)
        except Exception, exp:
            self.logger.error("Error running command(%s): %s" % (exe_cmd,
                                                                 str(exp)))
            raise exp

    def get_iqnname(self, output):
        iqnname = output[output.find("[")+1:output.find("]")]
        self.logger.debug("iqnname = %s" % iqnname)
        return iqnname

    def initialize_env(self, sky_ip):
        try:
            link = settings.DOWNLOAD_LINK.replace('%s', sky_ip)
            download_cmd = '%s(\\\"%s\\\",\\\"%s\\\")' % (settings.DOWNLOAD_BASE_CMD,
                                                          link,
                                                          settings.DOWNLOAD_LOCATION)
            output = self.run("powershell", settings.CREATE_DIR_CMD)
            self.logger.debug("initialize_env create_dir_cmd=[%s] output=[%s]"
                              % (settings.CREATE_DIR_CMD, output))
            output = self.run("powershell", download_cmd)
            self.logger.debug("Windows initialize_env download_cmd=[%s] output=[%s]"
                              % (download_cmd, output))
            output = self.run("powershell", settings.INSTALL_CONNECTOR_CMD)
            self.logger.debug("Windows initialize_env install_connector_cmd=[%s] output=[%s]"
                              % (settings.INSTALL_CONNECTOR_CMD, output))
            output = self.run("powershell", settings.RESTART_ISCI)
            self.logger.debug("Windows initialize_env restart_iscsi_cmd=[%s] output=[%s]"
                              % (settings.RESTART_ISCI, output))
            output = self.run("powershell", settings.GET_IQNNAME)
            self.logger.debug("Windows initialize_env get_iqnname_cmd=[%s] output=[%s]"
                              % (settings.GET_IQNNAME, output))
            return output
        except Exception, exp:
            self.logger.error("Error(%s): Windows initialize_env" % exp)
            raise Awsr2cError(exp)

    def cleanup(self):
        try:
            output = self.run("wmic", settings.WHERE_CLAUSE)
            self.logger.debug("Windows cleanup wmic output=[%s]" % str(output))
            if output == "":
                self.logger.error("The uninstaller path couldnt be found, returning")
                return
            path = self.__get_uninstaller_path(output)
            if not path:
                self.logger.debug("No connector installed, stopping the iscsi service")
                output = self.run("powershell", settings.STOP_ISCI)
                self.logger.debug("Windows cleanup stop_iscsi_cmd=[%s] output=[%s]"
                                  % (settings.STOP_ISCI, output))
                return output
            self.logger.debug("Windows cleanup path=[%s]" % path)
            unistaller_path = "%s%s" % (path,
                                        "unins000.exe")
            uninstall_cmd = "%s /SILENT /SUPPRESSMSGBOXES" % unistaller_path
            output = self.run("powershell", settings.STOP_UDSAGENT_CMD)
            self.logger.debug("Windows cleanup stop_udsagent_cmd=[%s] output=[%s]"
                              % (settings.STOP_UDSAGENT_CMD, output))
            output = self.run("powershell", uninstall_cmd)
            self.logger.debug("Windows cleanup uninstall_cmd=[%s] output=[%s]"
                              % (uninstall_cmd, output))
            output = self.run("powershell", settings.STOP_ISCI)
            self.logger.debug("Windows cleanup stop_iscsi_cmd=[%s] output=[%s]"
                              % (settings.STOP_ISCI, output))
            return output
        except Exception, exp:
            self.logger.error("Error(%s): Windows cleanup" % exp)
            raise Awsr2cError(exp)

    def __get_uninstaller_path(self, output):
        pathName = output[output.find('"'):].replace("UDSAgent.exe", "")
        path = pathName.splitlines()[0]
        if not path:
            return None
        path = path.rstrip().replace('"', '')
        path = path.replace("\\", "\\\\")
        path = path.replace(" ", "` ")
        return path
