'''
Created on Jun 12, 2015

@author: sonal.ojha
'''
from fabric.api import env
from fabric.operations import put, run
from fabric.network import disconnect_all


class SFTPClient(object):

    def __init__(self, host, user, pkey, logger):
        env.host_string = host
        env.user = user
        env.key_filename = pkey
        env.port = 22
        env.password = ""
        env.warn_only = False  # to handle exceptions yourself
        self.logger = logger

    def __del__(self):
        disconnect_all()

    def send_file(self, localpath, remotepath):
        put(localpath, remotepath, mode=0777, use_sudo=True)

    def create_dir(self, remotedir):
        cmd = 'mkdir -p %s' % remotedir
        self.logger.debug("create_dir :: cmd=[%s]" % cmd)
        return self.__run(cmd)

    def remove_dir(self, remotedir):
        cmd = 'rm -rf %s' % remotedir
        self.logger.debug("remove_dir :: cmd=[%s]" % cmd)
        return self.__run(cmd)

    def run_script(self, script, *args):
        cmd = None
        if script.endswith('.sh'):
            self.logger.debug("run_script :: running shell script %s" % script)
            cmd = "%s" % script
            for arg in args:
                cmd += " " + arg
        elif script.endswith('.py'):
            self.logger.debug("run_script :: running python script %s" % script)
            cmd = "python %s" % script
        if cmd:
            self.logger.debug("run_script :: command to run=[%s]" % cmd)
            return self.__run(cmd)

    def __run(self, command):
        x = run(command)
        if x.stderr != "":
            self.logger.error("__run :: stderr=[%s]" % x.stderr.decode("utf-8"))
            raise Exception(x.stderr)
        self.logger.debug("__run :: stdout=[%s]" % x.stdout.decode("utf-8"))
        return x.stdout
