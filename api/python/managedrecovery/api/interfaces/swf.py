'''
Created on May 27, 2015

@author: meghna.kale
'''

import boto.swf
import random
from django.conf import settings


class SWFInterface:

    # TODO : Please add these values in settings.py according
    # to your account before running the script.
    aws_key = settings.AWS_KEY
    aws_secret = settings.AWS_SECRET
    domain_name = settings.SWF_DOMAIN_NAME
    workflow_version = settings.SWF_WORKFLOW_VERSION
    workflow_type = 'EnableProtection_workflow'
    # creating a task list, creates a queue for execution.
    default_task_list = 'MainTaskList'

    def start_workflow(self, input_json):

        # Establish connection
        wf = boto.swf.layer1.Layer1(aws_access_key_id=self.aws_key,
                                    aws_secret_access_key=self.aws_secret)

        workflow_id = 'job-%s' % int(random.random() * 10000)

        print '===> starting workflow: %s' % workflow_id

        # So in this case, we want this execution to start
        # in the 'MainTaskList' queue

        execution = wf.start_workflow_execution(self.domain_name,
                                                workflow_id,
                                                self.workflow_type,
                                                self.workflow_version,
                                                self.default_task_list,
                                                input=input_json)
        print '===> start_workflow_execution returned %s' % execution
        return execution
