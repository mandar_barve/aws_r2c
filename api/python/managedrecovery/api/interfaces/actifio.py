'''
Created on June 14, 2015

@author: meghna.kale
'''
from managedrecovery.api.interfaces.base.baseHttp import BaseHttpInterface
from managedrecovery.common.awsr2cError.awsr2c_exception import Awsr2cError


class ActifioInterface(BaseHttpInterface):

    def __init__(self, sky_ip):
        BaseHttpInterface.__init__(self, sky_ip)

    def login(self, username, password, vendorkey):
        params = {'name': username, 'password': password,
                  'vendorkey': vendorkey}
        try:
            response_json = self.get('login', params)
            sessionid = response_json.get('sessionid')
            if sessionid is None:
                self.logger.error("Invalid user(%s)" % (username))
                raise Awsr2cError("Invalid user(%s)" % (username))
            self.logger.debug("Sessionid = %s" % sessionid)
            return sessionid
        except Exception, ex:
            self.logger.error("Failed to login into Sky(%s), with user(%s): Error:%s"
                              % (self.host, username, str(ex)))
            raise Awsr2cError("Failed to login into Sky(%s), with user(%s)"
                              % (self.host, username), stacktrace=ex)

    def add_host(self, sessionid, hostname, ipaddress, hosttype, iscsiname):
        params = {'sessionid': sessionid, 'hostname': hostname,
                  'ipaddress': ipaddress, 'type': hosttype,
                  'iscsiname': iscsiname}
        try:
            resp_json = self.post("task/mkhost", params)
            host_id = resp_json.get("result")
            self.logger.debug("Successfully added host(%s) with hostid:%s"
                              % (ipaddress, host_id))
            return host_id
        except Exception, ex:
            self.logger.error("Failed to add host(%s): Error:%s" % (hostname,
                                                                    str(ex)))
            raise Awsr2cError("Failed to add host(%s)" % hostname, stacktrace=ex)

    def app_discover(self, sessionid, hostname):
        params = {'sessionid': sessionid, 'host': hostname}
        resp_json = self.post("task/appdiscovery", params)
        status = resp_json.get('status')
        if status != 0:
            self.logger.error("Failed to discover applications for host(%s)"
                              % hostname)
            raise Awsr2cError("Failed to discover applications for host(%s)"
                              % hostname)
        self.logger.debug("Discovered host(%s) applications" % hostname)
        return True

    def list_hosts(self, sessionid, hostname=None, hasagent=None):
        params = {'sessionid': sessionid}
        if hasagent:
            params['filtervalue'] = {"hasagent": hasagent}
        if hostname:
            params['filtervalue'] = {"hostname": hostname}
        try:
            resp_json = self.get("info/lshost", params)
            hosts = resp_json.get("result")
            self.logger.debug("List of hosts(%s)" % str(hosts))
            return hosts
        except Exception, ex:
            self.logger.error("Failed to list Sky hosts: Error:%s" % (str(ex)))
            raise Awsr2cError("Failed to list Sky hosts", stacktrace=ex)

    def test_iscsi(self, sessionid, hostname):
        params = {'sessionid': sessionid, "host": hostname}
        resp_json = self.post("task/iscsitest", params)
        status = resp_json.get('status')
        self.logger.debug("host(%s) ISCSI test status=%s" % (hostname,
                                                             str(status)))
        if status != 0:
            raise Awsr2cError('ISCSI test failed for host(%s)' % hostname)
        return True

    # This is a call for on demand backup
    def add_backup(self, sessionid, app_id, policy_id):
        params = {'sessionid': sessionid, "app": app_id, 'policy': policy_id}
        try:
            resp_json = self.post('task/backup', params)
            self.logger.debug("add backup for app_id(%s), policy_id(%s) = %s"
                              % (app_id, policy_id, str(resp_json)))
            return resp_json
        except Exception, ex:
            self.logger.error("Failed to add backup for app_id(%s), policy_id(%s): Error:%s"
                              % (app_id, policy_id, str(ex)))
            raise Awsr2cError("Failed to add backup for app_id(%s), policy_id(%s)"
                              % (app_id, policy_id), stacktrace=ex)

    def protect_app(self, sessionid, app_id, slp, slt, description):
        params = {'sessionid': sessionid, "appid": app_id, 'slp': slp,
                  'slt': slt, 'description': description}
        try:
            resp_json = self.post('task/mksla', params)
            self.logger.debug("protect app response for app_id(%s) :%s"
                              % (app_id, str(resp_json)))
            if resp_json.get("status") != 0:
                raise Awsr2cError("Failed to protect application(%s)"
                                  % app_id)
            return True
        except Exception, ex:
            self.logger.error("Failed to protect application(%s): Error:%s"
                              % (app_id, str(ex)))
            raise Awsr2cError("Failed to protect application(%s):" % app_id,
                              stacktrace=ex)

    def list_applications(self, sessionid, hostid=None, appid=None, hostname=None):
        '''
        This would list all the applications. If hostid is provided it would
        list all applications for that host.
        '''
        params = {'sessionid': sessionid}
        if hostid:
            params['filtervalue'] = {"hostid": hostid}
        if appid:
            params['filtervalue'] = {"id": appid}
        if hostname:
            params['filtervalue'] = {"hostname": hostname}
        try:
            resp_json = self.get("info/lsapplication", params)
            apps = resp_json.get("result")
            self.logger.debug("List of  applications:%s" % (str(apps)))
            return apps
        except Exception, ex:
            self.logger.error("Failed to list applications: Error:%s"
                              % (str(ex)))
            raise Awsr2cError("Failed to list applications:", stacktrace=ex)

    def list_policy(self, sessionid):
        params = {'sessionid': sessionid}
        try:
            resp_json = self.get('info/lspolicy', params)
            policies = resp_json.get("result")
            self.logger.debug("List of policies:%s" % (str(policies)))
            return policies
        except Exception, ex:
            self.logger.error("Failed to list policies: Error:%s" % (str(ex)))
            raise Awsr2cError("Failed to list policies", stacktrace=ex)

    def create_group(self, sessionid, groupname, description):
        params = {'sessionid': sessionid, 'name': groupname,
                  'description': description}
        try:
            resp_json = self.post('task/mkgroup', params)
            group_id = resp_json.get("result")
            self.logger.debug("Created Group(%s)" % group_id)
            return group_id
        except Exception, ex:
            self.logger.error("Failed to create group(%s): Error:%s"
                              % (groupname, str(ex)))
            raise Awsr2cError("Failed to create group(%s)" % groupname,
                              stacktrace=ex)

    def list_groups(self, sessionid, group_name=None):
        params = {'sessionid': sessionid}
        if group_name:
            params['filtervalue'] = {"name": group_name}
        try:
            resp_json = self.get("info/lsgroup", params)
            groups = resp_json.get("result")
            self.logger.debug("List of Groups = %s" % str(groups))
            return groups
        except Exception, ex:
            self.logger.error("Failed to list groups: Error:%s" % (str(ex)))
            raise Awsr2cError("Failed to list groups", stacktrace=ex)

    def delete_group(self, sessionid, group_id):
        params = {'sessionid': sessionid, 'argument': group_id}
        try:
            resp_json = self.post('task/rmgroup', params)
            if resp_json.get("status") != 0:
                self.logger.error("Failed to remove Sky group(%s): status:%s"
                                  % (group_id, str(resp_json.get("status"))))
                raise Awsr2cError("Failed to remove Sky group(%s)" % group_id)
            return True
        except Exception, ex:
            self.logger.error("Failed to remove Sky group(%s): Error:%s"
                              % (group_id, str(ex)))
            raise Awsr2cError("Failed to remove Sky group(%s)" % group_id,
                              stacktrace=ex)

    def add_group_member(self, sessionid, appid, groupid):
        params = {'sessionid': sessionid, 'appid': appid, 'groupid': groupid}
        try:
            resp_json = self.post('task/mkgroupmember', params)
            memberid = resp_json.get("result")
            self.logger.debug("App(%s) added to Sky group(%s), memberid=%s"
                              % (appid, groupid, memberid))
            return memberid
        except Exception, ex:
            self.logger.error("Failed to add app(%s) to group(%s): Error:%s"
                              % (appid, groupid, str(ex)))
            raise Awsr2cError("Failed to add app(%s) to group(%s):" % (appid, groupid),
                              stacktrace=ex)

    def list_group_members(self, sessionid, group_id=None, app_id=None):
        params = {'sessionid': sessionid}
        if app_id:
            params['filtervalue'] = {"appid": app_id}
        if group_id:
            params['filtervalue'] = {"groupid": group_id}
        try:
            resp_json = self.get("info/lsgroupmember", params)
            members = resp_json.get("result")
            self.logger.debug("List of members = %s" % str(members))
            return members
        except Exception, ex:
            self.logger.error("Failed to list group members : Error:%s"
                              % (str(ex)))
            raise Awsr2cError("Failed to list group members", stacktrace=ex)

    def delete_group_member(self, sessionid, group_member_id):
        params = {'sessionid': sessionid, 'argument': group_member_id}
        try:
            resp_json = self.post("task/rmgroupmember", params)
            if resp_json.get("status") != 0:
                self.logger.error("Failed to remove group member(%s)"
                                  % group_member_id)
                raise Awsr2cError("Failed to remove group member(%s)"
                                  % group_member_id)
            return True
        except Exception, ex:
            self.logger.error("Failed to delete group member(%s) : Error:%s"
                              % (group_member_id, str(ex)))
            raise Awsr2cError("Failed to delete group member(%s)" % group_member_id,
                              stacktrace=ex)

    def protect_group(self, sessionid, group_id, slp, slt, description):
        params = {'sessionid': sessionid, 'group': group_id, 'slp': slp,
                  'slt': slt, 'description': description}
        try:
            resp_json = self.post("task/mksla", params)
            return resp_json
        except Exception, ex:
            self.logger.error("Failed to protect a group(%s) : Error:%s"
                              % (group_id, str(ex)))
            raise Awsr2cError("Failed to protect a group(%s)" % group_id,
                              stacktrace=ex)

    def unprotect_group(self, sessionid, group_id):
        params = {'sessionid': sessionid, 'argument': group_id}
        try:
            resp_json = self.post("task/rmsla", params)
            if resp_json.get("status") != 0:
                raise Awsr2cError("Failed to unprotect Sky group(%s)"
                                  % group_id)
            return True
        except Exception, ex:
            self.logger.error("Failed to unProtect group(%s) : Error:%s"
                              % (group_id, str(ex)))
            raise Awsr2cError("Failed to unProtect group(%s)" % group_id,
                              stacktrace=ex)

    def list_sla(self, sessionid, app_id=None):
        params = {'sessionid': sessionid}
        if app_id:
            params['filtervalue'] = {"appid": app_id}
        try:
            resp_json = self.get("info/lssla", params)
            slas = resp_json.get("result")
            self.logger.debug("SLAs list = %s" % str(slas))
            return slas
        except Exception, ex:
            self.logger.error("Failed to list SLAs : Error:%s" % (str(ex)))
            raise Awsr2cError("Failed to list SLAs", stacktrace=ex)

    def list_slt(self, sessionid):
        params = {'sessionid': sessionid}
        try:
            resp_json = self.get("info/lsslt", params)
            slts = resp_json.get("result")
            self.logger.debug("SLTs list = %s" % str(slts))
            return slts
        except Exception, ex:
            self.logger.error("Failed to list SLTs : Error:%s" % (str(ex)))
            raise Awsr2cError("Failed to list SLTs", stacktrace=ex)

    def list_slp(self, sessionid):
        params = {'sessionid': sessionid}
        try:
            resp_json = self.get("info/lsslp", params)
            slps = resp_json.get("result")
            self.logger.debug("SLPs list = %s" % str(slps))
            return slps
        except Exception, ex:
            self.logger.error("Failed to list SLPs : Error:%s" % (str(ex)))
            raise Awsr2cError("Failed to list SLPs", stacktrace=ex)

    def unprotect_app(self, sessionid, sla_id):
        params = {'sessionid': sessionid, 'argument': sla_id}
        try:
            resp_json = self.post("task/rmsla", params)
            if resp_json.get("status") != 0:
                raise Awsr2cError("Failed to un-protect app for sla_id(%s)"
                                  % sla_id)
            return True
        except Exception, ex:
            self.logger.error("Failed to un-protect app for sla_id(%s): Error:%s"
                              % (sla_id, str(ex)))
            raise Awsr2cError("Failed to un-protect app for sla_id(%s)" % sla_id,
                              stacktrace=ex)

    def delete_host(self, sessionid, hostid):
        params = {'sessionid': sessionid, 'argument': hostid}
        try:
            resp_json = self.post("task/rmhost", params)
            if resp_json.get("status") != 0:
                raise Awsr2cError("Failed to remove host(%s) from Sky"
                                  % hostid)
            return True
        except Exception, ex:
            self.logger.error("Failed to remove host(%s): Error:%s"
                              % (hostid, str(ex)))
            raise Awsr2cError("Failed to remove host(%s)" % hostid,
                              stacktrace=ex)

    def list_disk_pool_stat(self, sessionid, startdate, **arg):
        arg.update({"stattime >=": startdate})
        params = {'sessionid': sessionid, "filtervalue": arg}
        try:
            resp_json = self.get("info/lsdiskpoolstat", params)
            dps = resp_json.get("result")
            self.logger.debug("Disk Pool Stat list = %s" % str(dps))
            return dps
        except Exception, ex:
            self.logger.error("Failed to List disk pool stat: Error:%s"
                              % (str(ex)))
            raise Awsr2cError("Failed to List disk pool stat", stacktrace=ex)

    def list_dedup_pool_stat(self, sessionid, startdate, **arg):
        arg.update({"stattime >=": startdate})
        params = {'sessionid': sessionid, "filtervalue": arg}
        try:
            resp_json = self.get("info/lsdeduppoolstat", params)
            dedups = resp_json.get("result")
            self.logger.debug("Dedup Pool Stat list = %s" % str(dedups))
            return dedups
        except Exception, ex:
            self.logger.error("Failed to List dedup pool stat: Error:%s"
                              % (str(ex)))
            raise Awsr2cError("Failed to List dedup pool stat", stacktrace=ex)

    def list_snapshot_pool_stat(self, sessionid, startdate, **arg):
        arg.update({"stattime >=": startdate})
        params = {'sessionid': sessionid, "filtervalue": arg}
        try:
            resp_json = self.get("info/lssnappoolstat", params)
            ssps = resp_json.get("result")
            self.logger.debug("Snapshot Pool Stat list = %s" % str(ssps))
            return ssps
        except Exception, ex:
            self.logger.error("Failed to List snapshot pool stat: Error:%s"
                              % (str(ex)))
            raise Awsr2cError("Failed to List snapshot pool stat", stacktrace=ex)

    def list_mdl_stat(self, sessionid, startdate, **arg):
        arg.update({"stattime >=": startdate})
        params = {'sessionid': sessionid, "filtervalue": arg}
        try:
            resp_json = self.get("info/lsmdlstat", params)
            mdls = resp_json.get("result")
            self.logger.debug("MDL Stat list = %s" % str(mdls))
            return mdls
        except Exception, ex:
            self.logger.error("Failed to list mdl stat: Error:%s" % (str(ex)))
            raise Awsr2cError("Failed to list mdl stat", stacktrace=ex)

    def backup_stat(self, sessionid, **arg):
        params = {'sessionid': sessionid, "filtervalue": arg}
        try:
            self.logger.debug("params = %s" % params)
            resp_json = self.get("info/lsbackup", params)
            images = resp_json.get("result")
            self.logger.debug("Images = %s" % str(images))
            return images
        except Exception, ex:
            self.logger.error("Failed to get backup stat: Error:%s"
                              % (str(ex)))
            raise Awsr2cError("Failed to get backup stat", stacktrace=ex)

    def mount_image(self, sessionid, imageid, hostname, **args):
        params = {'sessionid': sessionid, "image": imageid,
                  "host": hostname}
        params.update(args)
        try:
            self.logger.debug("Mount image[%s] on host[%s]" % (imageid,
                                                               hostname))
            resp_json = self.post("task/mountimage", params)
            self.logger.debug("resp_json = %s" % params)
            if resp_json.get("status") != 0:
                raise Awsr2cError("Failed to Mount image[%s] on host[%s]"
                                  % (imageid, hostname))
            return True
        except Exception, ex:
            self.logger.error("Failed to mount image[%s] on host[%s]: Error:%s"
                              % (imageid, hostname, str(ex)))
            raise Awsr2cError("Failed to mount image[%s] on host[%s]"
                              % (imageid, hostname), stacktrace=ex)

    def restore_image(self, sessionid, imageid):
        params = {'sessionid': sessionid, "image": imageid}
        try:
            self.logger.debug("Restore image[%s]" % imageid)
            resp_json = self.post("task/restoreimage", params)
            self.logger.debug("resp_json = %s" % params)
            if resp_json.get("status") != 0:
                raise Awsr2cError("Failed to Restore image[%s]" % imageid)
            return True
        except Exception, ex:
            self.logger.error("Failed to Restore image[%s]: Error:%s" %
                              (imageid, str(ex)))
            raise Awsr2cError("Failed to Restore image[%s]" % imageid,
                              stacktrace=ex)

    def failback(self, sessionid, appid):
        params = {'sessionid': sessionid, "id": appid}
        try:
            self.logger.debug("failback for app[%s]" % appid)
            resp_json = self.post("task/failback", params)
            self.logger.debug("resp_json = %s" % params)
            if resp_json.get("status") != 0:
                raise Awsr2cError("Failed to failback app[%s]" % appid)
            return True
        except Exception, ex:
            self.logger.error("Failed to failback app[%s]: Error:%s" %
                              (appid, str(ex)))
            raise Awsr2cError("Failed to failback app[%s]" % appid,
                              stacktrace=ex)


