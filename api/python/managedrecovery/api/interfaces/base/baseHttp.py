import logging
import logging.config
from managedrecovery.common.logger.awsr2c_logging import Awsr2clogging
import requests
import urllib
from django.conf import settings
from managedrecovery.common.awsr2cError.awsr2c_exception import Awsr2cError
import json


class BaseHttpInterface():

    def __init__(self,  sky_ip):
        self.host = sky_ip
        logconfig_path = settings.LOGGER_CONFIG_PATH
        logging.config.fileConfig(logconfig_path)
        self.logger = Awsr2clogging.getLogger("awsr2c.actifio", Awsr2clogging.COMPONENT_ACTIFIO)
        self.base_url = "https://%s/actifio/api" % self.host

    def get(self, url, params):
        request_url = self.__form_complete_url(url, params)
        self.logger.debug("GET Request url = %s" % request_url)
        response = requests.get(request_url, verify=False)
        self.logger.debug("GET Response = %s" % response.content)
        return self.__check_response(response)

    def delete(self, url, params):
        request_url = self.__form_complete_url(url, params)
        self.logger.debug("DELETE Request url = %s" % request_url)
        response = requests.delete(request_url, verify=False)
        self.logger.debug("DELETE Response = %s" % response.content)
        return self.__check_response(response)

    def post(self, url, params=None, body=None):
        request_url = self.__form_complete_url(url, params)
        self.logger.debug("POST Request url = %s" % request_url)
        response = requests.post(request_url, data=body, verify=False)
        self.logger.debug("POST Response = %s" % response.content)
        return self.__check_response(response)

    def __form_complete_url(self, url, params):
        filterval = None
        if params.get("filtervalue"):
            filterval = params.pop("filtervalue")
        query = urllib.urlencode(params)
        if filterval:
            self.logger.debug(str(filterval))
            subquery = urllib.quote_plus('&'.join([each+'='+ str(filterval[each]) for each in filterval if each]))
            query = "%s&filtervalue=%s" % (query, subquery)
        if not query:
            return "%s/%s" % (self.base_url, url)
        return "%s/%s?%s" % (self.base_url, url, query)

    def __check_response(self, response):
        if response.status_code == 500:
            r_json = json.loads(response.content)
            self.logger.debug("Internal Server Error, r_json=%s" % r_json)
            if r_json.get("errormessage"):
                raise Awsr2cError(r_json.get("errormessage"))
            raise Awsr2cError(r_json.split(",")[1])
        elif response.status_code == 200:
            self.logger.debug("Success response json=%s" % response.json())
            return response.json()
        else:
            try:
                r_json = response.json()
            except ValueError:
                raise Awsr2cError(response.content)
            self.logger.debug("response json = %s" % r_json)
            if r_json.get("errormessage"):
                raise Awsr2cError(r_json.get("errormessage"))
            return r_json
