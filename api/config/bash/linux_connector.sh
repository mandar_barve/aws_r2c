#!/bin/bash

if [[ -z $1 ]]; then
   echo "IP not provided"
   exit 1
fi
IPADDRESS=$1
echo $IPADDRESS
sudo yum -y install iscsi-initiator-utils wget redhat-lsb-core
release=`sudo lsb_release -r | awk '{print $2}' | bc`
echo $release
retval=`echo "$release < 7" | bc`
if [[ $retval -eq 1 ]]; then
   echo "using service command"
   sudo chkconfig iscsid on
   sudo service iscsid force-start
   sudo service iscsid status
else
   echo "using systemctl"
   sudo systemctl enable iscsid.service
   sudo systemctl start iscsid.service
   sudo systemctl status iscsid.service
fi
wget -O /tmp/managedrecovery/connector.rpm http://$IPADDRESS/connector-Linux-latestversion.rpm
sudo yum install -y /tmp/managedrecovery/connector.rpm
iqnname=$(cat /etc/iscsi/initiatorname.iscsi)
echo $iqnname
