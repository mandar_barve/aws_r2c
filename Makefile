TEST_SUBDIRS := api common
BUILD_SUBDIRS := $(TEST_SUBDIRS)
SCRIPT_SUBDIRS := automation
BUILD_SCRIPTSSUBDIRS := $(SCRIPT_SUBDIRS)


build: build_python

build_python:
	$(foreach dir,$(BUILD_SUBDIRS), $(MAKE) -C $(dir) build;)

buildrpm:
	$(foreach dir,$(BUILD_SUBDIRS), $(MAKE) -C $(dir) buildrpm;)

install: installrpm

installrpm:
	$(foreach dir,$(BUILD_SUBDIRS), $(MAKE) -C $(dir) installrpm;)
	$(foreach dir,$(BUILD_SCRIPTSSUBDIRS), $(MAKE) -C $(dir) installscripts;)

installscripts:
	$(foreach dir,$(BUILD_SCRIPTSSUBDIRS), $(MAKE) -C $(dir) installscripts;)

test:
	$(foreach dir,$(TEST_SUBDIRS), $(MAKE) -C $(dir) test;)

