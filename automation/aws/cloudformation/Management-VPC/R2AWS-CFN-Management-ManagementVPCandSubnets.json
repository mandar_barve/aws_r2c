{
  "AWSTemplateFormatVersion": "2010-09-09",
  "Description": "Sungard AS - This CFN will create the VPC and subnets.",
  "Parameters": {
    "VpcCIDR": {
      "Description": "The CIDR of new VPC to deploy the AWS resources.",
      "Type": "String",
      "MinLength": "9",
      "MaxLength": "18",
      "AllowedPattern": "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})/(\\d{1,2})",
      "ConstraintDescription": "must be a valid IP CIDR range of the form x.x.x.x/x."
    },
    "SubnetIpBlocks": {
      "Description": "Comma-delimited list of four CIDR blocks, first two will be created as private subnets and last two will created as public subnets.",
      "Type": "CommaDelimitedList"
    },
	"NATNodeInstanceType" : {
      "Description" : "Instance type for NAT nodes.",
      "Type" : "String"
    },
	"KeyName" : {
      "Description" : "Name of an existing EC2 KeyPair to enable SSH access to the instances",
      "Type": "AWS::EC2::KeyPair::KeyName"
    }
  },
  "Mappings": {
    "Region2AZ": {
      "us-east-1": {
        "AZ": [ "us-east-1a", "us-east-1b", "us-east-1c" ]
      },
      "us-west-1": {
        "AZ": [ "us-west-1a", "us-west-1b", "us-west-1b" ]
      },
      "us-west-2": {
        "AZ": [ "us-west-2a", "us-west-2b", "us-west-2c" ]
      },
      "eu-west-1": {
        "AZ": [ "eu-west-1a", "eu-west-1b", "eu-west-1c" ]
      }
    },
	"NATAMI" : {
      "us-east-1" : { "AMI" : "ami-12663b7a" },
      "us-west-2" : { "AMI" : "ami-a540a5e1" },
      "us-west-1" : { "AMI" : "ami-4dbf9e7d" },
      "eu-west-1" : { "AMI" : "ami-25158352" }
    }
  },
  "Resources": {
    "ManagementVPC": {
      "Type": "AWS::EC2::VPC",
      "Properties": {
        "CidrBlock": { "Ref": "VpcCIDR" },
		"Tags" : [ {"Key" : "Name", "Value" : "SungardAS-ManagementVPC"} ]
      }
    },
    "PrivateSubnet01": {
      "Type": "AWS::EC2::Subnet",
	  "Properties": {
	    "AvailabilityZone": { "Fn::Select" : [ "0", { "Fn::FindInMap" : [ "Region2AZ", { "Ref" : "AWS::Region" }, "AZ" ] } ] },
        "VpcId": { "Ref": "ManagementVPC" },
        "CidrBlock": { "Fn::Select" : [ "0", {"Ref" : "SubnetIpBlocks"} ] },
		"Tags" : [ {"Key" : "Name", "Value" : "SungardAS-PrivateSubnet01"} ]
      }
    },
    "PrivateSubnet02": {
      "Type": "AWS::EC2::Subnet",
	  "Properties": {
	    "AvailabilityZone": { "Fn::Select" : [ "1", { "Fn::FindInMap" : [ "Region2AZ", { "Ref" : "AWS::Region" }, "AZ" ] } ] },
        "VpcId": { "Ref": "ManagementVPC" },
        "CidrBlock": { "Fn::Select" : [ "1", {"Ref" : "SubnetIpBlocks"} ] },
		"Tags" : [ {"Key" : "Name", "Value" : "SungardAS-PrivateSubnet02"} ]
      }
    },
	"PublicSubnet01": {
      "Type": "AWS::EC2::Subnet",
	  "Properties": {
	    "AvailabilityZone": { "Fn::Select" : [ "0", { "Fn::FindInMap" : [ "Region2AZ", { "Ref" : "AWS::Region" }, "AZ" ] } ] },
        "VpcId": { "Ref": "ManagementVPC" },
        "CidrBlock": { "Fn::Select" : [ "2", {"Ref" : "SubnetIpBlocks"} ] },
		"Tags" : [ {"Key" : "Name", "Value" : "SungardAS-PublicSubnet01"} ]
      }
    },
	"PublicSubnet02": {
      "Type": "AWS::EC2::Subnet",
	  "Properties": {
	    "AvailabilityZone": { "Fn::Select" : [ "1", { "Fn::FindInMap" : [ "Region2AZ", { "Ref" : "AWS::Region" }, "AZ" ] } ] },
        "VpcId": { "Ref": "ManagementVPC" },
        "CidrBlock": { "Fn::Select" : [ "3", {"Ref" : "SubnetIpBlocks"} ] },
		"Tags" : [ {"Key" : "Name", "Value" : "SungardAS-PublicSubnet02"} ]
      }
    },
	"PrivateRouteTable01": {
      "Type": "AWS::EC2::RouteTable",
      "Properties": {
        "VpcId": { "Ref": "ManagementVPC" },
		"Tags" : [ {"Key" : "Name", "Value" : "SungardAS-PrivateRouteTable01"} ]
      }
    },
    "SubnetRouteTableAssociation01": {
      "Type": "AWS::EC2::SubnetRouteTableAssociation",
      "Properties": {
        "SubnetId": { "Ref": "PrivateSubnet01" },
        "RouteTableId": { "Ref": "PrivateRouteTable01" }
      }
    },
    "PrivateRouteTable02": {
      "Type": "AWS::EC2::RouteTable",
      "Properties": {
        "VpcId": { "Ref": "ManagementVPC" },
		"Tags" : [ {"Key" : "Name", "Value" : "SungardAS-PrivateRouteTable02"} ]
      }
    },
    "SubnetRouteTableAssociation02": {
      "Type": "AWS::EC2::SubnetRouteTableAssociation",
      "Properties": {
        "SubnetId": { "Ref": "PrivateSubnet02" },
        "RouteTableId": { "Ref": "PrivateRouteTable02" }
      }
    },
	"PublicRouteTable01": {
      "Type": "AWS::EC2::RouteTable",
      "Properties": {
        "VpcId": { "Ref": "ManagementVPC" },
		"Tags" : [ {"Key" : "Name", "Value" : "SungardAS-PublicRouteTable01"} ]
      }
    },
    "SubnetRouteTableAssociation03": {
      "Type": "AWS::EC2::SubnetRouteTableAssociation",
      "Properties": {
        "SubnetId": { "Ref": "PublicSubnet01" },
        "RouteTableId": { "Ref": "PublicRouteTable01" }
      }
    },
	"PublicRouteTable02": {
      "Type": "AWS::EC2::RouteTable",
      "Properties": {
        "VpcId": { "Ref": "ManagementVPC" },
		"Tags" : [ {"Key" : "Name", "Value" : "SungardAS-PublicRouteTable02"} ]
      }
    },
    "SubnetRouteTableAssociation04": {
      "Type": "AWS::EC2::SubnetRouteTableAssociation",
      "Properties": {
        "SubnetId": { "Ref": "PublicSubnet02" },
        "RouteTableId": { "Ref": "PublicRouteTable02" }
      }
    },
	"InternetGateway": {
      "Type": "AWS::EC2::InternetGateway",
	  "Properties": {
	    "Tags" : [ {"Key" : "Name", "Value" : "SungardAS-IGW"} ]  
	  }
    },
    "AttachGateway": {
      "Type": "AWS::EC2::VPCGatewayAttachment",
      "Properties": {
        "VpcId": {"Ref": "ManagementVPC"},
        "InternetGatewayId": {"Ref": "InternetGateway"}
      }
    },
	"PublicRoute01": {
      "Type": "AWS::EC2::Route",
      "DependsOn": "AttachGateway",
      "Properties": {
        "RouteTableId": {"Ref": "PublicRouteTable01"},
        "DestinationCidrBlock": "0.0.0.0/0",
        "GatewayId": {"Ref": "InternetGateway"}
      }
    },
	"PublicRoute02": {
      "Type": "AWS::EC2::Route",
      "DependsOn": "AttachGateway",
      "Properties": {
        "RouteTableId": {"Ref": "PublicRouteTable02"},
        "DestinationCidrBlock": "0.0.0.0/0",
        "GatewayId": {"Ref": "InternetGateway"}
      }
    },
	"InternetRoute01": {
      "Type": "AWS::EC2::Route",
	  "DependsOn": "NATInstance",
      "Properties": {
        "RouteTableId": {"Ref" : "PrivateRouteTable01"},
        "DestinationCidrBlock": "0.0.0.0/0",
        "InstanceId": {"Ref": "NATInstance"}
      }
    },
	"InternetRoute02": {
      "Type": "AWS::EC2::Route",
	  "DependsOn": "NATInstance",
      "Properties": {
        "RouteTableId": {"Ref" : "PrivateRouteTable02"},
        "DestinationCidrBlock": "0.0.0.0/0",
        "InstanceId": {"Ref": "NATInstance"}
      }
    },
	"NATEIP" : {
      "Type" : "AWS::EC2::EIP",
	  "DependsOn" : ["ManagementVPC", "NATInstance", "AttachGateway"],
      "Properties" : {
        "Domain" : "vpc",
        "InstanceId" : { "Ref" : "NATInstance" }
      }
    },
	"NATSecurityGroup" : {
      "Type" : "AWS::EC2::SecurityGroup",
      "Properties" : {
        "GroupDescription" : "Rules for allowing access to NAT node",
		"Tags" : [ {"Key" : "Name", "Value" : "SungardAS-NATSecurityGroup"} ],
        "VpcId" : { "Ref" : "ManagementVPC" },
        "SecurityGroupIngress" : [
           { "IpProtocol" : "tcp", "FromPort" : "22",  "ToPort" : "22",  "CidrIp" : "0.0.0.0/0" } ,
           { "IpProtocol" : "-1", "FromPort" : "0",  "ToPort" : "65535",  "CidrIp" : { "Ref" : "VpcCIDR" }} ],
        "SecurityGroupEgress" : [
           { "IpProtocol" : "-1", "FromPort" : "0", "ToPort" : "65535", "CidrIp" : "0.0.0.0/0" } ]
      }
    },
	"NATInstance" : {
      "Type" : "AWS::EC2::Instance",
      "Properties" : {
        "InstanceType" : { "Ref" : "NATNodeInstanceType" } ,
        "KeyName" : { "Ref" : "KeyName" },
	    "SubnetId" : { "Ref" : "PublicSubnet01" },
        "SourceDestCheck" : "false",
        "ImageId" : { "Fn::FindInMap" : [ "NATAMI", { "Ref" : "AWS::Region" }, "AMI" ]},
        "SecurityGroupIds" : [{ "Ref" : "NATSecurityGroup" }],
        "Tags" : [{ "Key" : "Name", "Value" : "SungardAS-NAT01" }],
        "UserData" : { "Fn::Base64" : { "Fn::Join" : ["", [
          "#!/bin/bash -v\n",
		  "# Configure iptables\n",
	      "/sbin/iptables -t nat -A POSTROUTING -o eth0 -s 0.0.0.0/0 -j MASQUERADE\n",
	      "/sbin/iptables-save > /etc/sysconfig/iptables\n",
          "# Configure ip forwarding and redirects\n",
          "echo 1 >  /proc/sys/net/ipv4/ip_forward && echo 0 >  /proc/sys/net/ipv4/conf/eth0/send_redirects\n",
	      "mkdir -p /etc/sysctl.d/\n",
	      "cat <<EOF > /etc/sysctl.d/nat.conf\n",
	      "net.ipv4.ip_forward = 1\n",
	      "net.ipv4.conf.eth0.send_redirects = 0\n",
	      "EOF\n"
        ]]}}
      }
    }
  },
  "Outputs":{  
    "VPCID":{  
      "Description":"VPC ID",
      "Value":{ "Ref":"ManagementVPC" }
    },
	"PrivateRouteTable01ID":{  
      "Description":"Private Route Table01 ID",
      "Value":{ "Ref":"PrivateRouteTable01" }
    },
	"PrivateRouteTable02ID":{  
      "Description":"Private Route Table02 ID",
      "Value":{ "Ref":"PrivateRouteTable02" }
    },
	"PublicRouteTable01ID":{  
      "Description":"Public Route Table01 ID",
      "Value":{ "Ref":"PublicRouteTable01" }
    },
	"PublicRouteTable02ID":{  
      "Description":"Public Route Table02 ID",
      "Value":{ "Ref":"PublicRouteTable02" }
    },
	"PrivateSubnet01ID":{  
      "Description":"Private Subnet01 ID",
      "Value":{ "Ref":"PrivateSubnet01" }
    },
	"PrivateSubnet02ID":{  
      "Description":"Private Subnet02 ID",
      "Value":{ "Ref":"PrivateSubnet02" }
    },
	"PublicSubnet01ID":{  
      "Description":"Public Subnet01 ID",
      "Value":{ "Ref":"PublicSubnet01" }
    },
	"PublicSubnet02ID":{  
      "Description":"Public Subnet02 ID",
      "Value":{ "Ref":"PublicSubnet02" }
    }
  }
}
