#!/usr/bin/env python
import urllib,getpass
import requests
import json
import os
from argparse import ArgumentParser
from prettytable import PrettyTable
from datetime import datetime
CURRENT_DATE = datetime.now().strftime('%Y-%m-%d')


API_URL = 'http://%s/api/1.0'
MAPPING = {'skyIP': 'sky_ip',
           'sessionID': 'sessionid',
           'hostName': 'hostname',
           'skyUser': 'username',
           'skyPassword': 'password',
           'skyVenderkey': 'vendorkey',
           'startDate': 'startdate',
           'hostName': 'hostname',
           'appName': 'appname'
           }

LOGIN_ARGS = ['skyUser', 'skyPassword', 'skyVenderkey', 'skyIP']
DISK_USAGE_ARGS = ['sessionID', 'skyIP','startDate']
AGGREGATE_DISK_USAGE_ARGS = ['hostID','protectionDomainName','sessionID','skyIP']
DEDUP_USAGE_ARGS = ['sessionID', 'skyIP','startDate','hostName','appName']
SNAPSHOT_USAGE_ARGS = ['sessionID', 'skyIP','hostName','appName','startDate']
MDL_USAGE_ARGS = ['sessionID', 'skyIP','startDate','hostName','appName']
BACKUP_ARGS = ['sessionID', 'skyIP','hostName','appName']
PROTECTED_HOST_ARGS = ['sessionID', 'skyIP']
TOTAL_PROTECTED_HOST_ARGS = ['sessionID', 'skyIP']
TOTAL_APP_SIZE_ARGS = ['sessionID', 'skyIP','startDate']

LOGIN_URL = '/login/'
DISK_USAGE_URL = '/usage/diskUsage/'
AGGREGATE_DISK_USAGE_URL = '/usage/aggregateDiskUsage/'
DEDUP_USAGE_URL = '/usage/dedupUsage/'
SNAPSHOT_USAGE_URL = '/usage/snapshotUsage/'
MDL_USAGE_URL = '/usage/mdlUsage/'
BACKUP_URL = '/usage/backupStat/'
PROTECTED_HOST_URL = '/usage/protectHosts/'
TOTAL_PROTECTED_HOST_URL = '/usage/protectHostCount/'
TOTAL_APP_SIZE_URL = '/usage/totalProtectionAppSize/'

ACTIONS = {'disk': (DISK_USAGE_URL, DISK_USAGE_ARGS, 'get'),
           'aggregateDisk': (AGGREGATE_DISK_USAGE_URL, AGGREGATE_DISK_USAGE_ARGS, 'get'),
           'snapshot': (SNAPSHOT_USAGE_URL, SNAPSHOT_USAGE_ARGS, 'get'),
           'dedup': (DEDUP_USAGE_URL, DEDUP_USAGE_ARGS, 'get'),
           'mdl': (MDL_USAGE_URL, MDL_USAGE_ARGS, 'get'),
           'backup': (BACKUP_URL, BACKUP_ARGS, 'get'),
           'protectedAppSize': (TOTAL_APP_SIZE_URL, TOTAL_APP_SIZE_ARGS, 'get'),
           'protectedHostCount': (TOTAL_PROTECTED_HOST_URL, TOTAL_PROTECTED_HOST_ARGS, 'get'),
           'protectedHost': (PROTECTED_HOST_URL, PROTECTED_HOST_ARGS, 'get'),
           'login': (LOGIN_URL, LOGIN_ARGS, 'get'),
           }


# -------------------------------------------------------------------------
def send_req(url, body, method):
    response = {}
    try:
        if method == 'get':
            resp = requests.get(url, verify=False)
            return resp.content
    except ValueError, err:
        raise Exception(resp.content)
    return response


# -------------------------------------------------------------------------
def get_body(arg, params):
    body = dict()
    for param in params:
        for arg_name, r_param_name in MAPPING.items():
            if arg_name != param:
                continue
            body[r_param_name] = arg.get(arg_name)
            if not arg.get(arg_name):
                raise Exception("%s not provided" % arg_name)
            else:
                if arg.get(arg_name).lower() == "yes":
                    body[r_param_name] = True
                if arg.get(arg_name).lower() == "no":
                    body[r_param_name] = False
    return body


# -------------------------------------------------------------------------
def get_queryurl(arg, params):
    body = dict()
    for param in params:
        for arg_name, r_param_name in MAPPING.items():
            if arg_name != param:
                continue
            body[r_param_name] = arg.get(arg_name)
    query = urllib.urlencode(body)
    return query


# -------------------------------------------------------------------------
def get_session_id(arg, url):
    query_url = get_queryurl(arg, LOGIN_ARGS)
    final_url = "%s%s?%s" % (url, LOGIN_URL, query_url)
    sessionid = send_req(final_url, None, 'get')
    return sessionid.strip('"')

# -------------------------------------------------------------------------
def main(arg):
    try:
        url = API_URL % str(arg.get('awsr2cApiHost'))
        for action, params in ACTIONS.items():
            if arg.get('action') != action:
                continue
            if not arg.get('sessionID'):
                if action != "login":
                    print "\033[32margument sessionID not provided, hence logging in Sky\033[0m"
                arg["sessionID"] = get_session_id(arg, url)
                print "\033[33msessionID =\033[0m", arg["sessionID"]
	    if arg.get('action') == 'login':
                print "\033[32mPerforming action= %s\033[0m" % ("get login sessionid.")
	    else:
                print "\033[32mPerforming action= %s\033[0m" % (action + " usage details.")

            action_url, action_arg_names, method = params
            body = get_body(arg, action_arg_names)
            #print "\033[33mJSON=%s\033[0m" % body
            if method == 'get' or method == 'delete':
                query_url = get_queryurl(arg, action_arg_names)
                final_url = "%s%s?%s" % (url, action_url, query_url)
                body = None
            else:
                final_url = "%s%s" % (url, action_url)
            response = send_req(final_url, body, method)
            return response
    except Exception, e:
        print "\033[31m%s\033[0m" % str(e)
        raise e

# -------------------------------------------------------------------------
def cleanupdata(data):
    if data.get('poolname') == 'act_ded_pool000':
	data['poolname'] = 'dedup pool stat'
    if data.get('poolname') == 'act_per_pool000':
	data['poolname'] = 'snapshot pool stat'
    if data.get('poolname') == 'act_pri_pool000':
	data['poolname'] = 'primary pool stat'
    return data	
    
# -------------------------------------------------------------------------
def make_table(data, actiontype):
    data = str(data)
    if data.find('Error') > -1: return data
    data = eval(data)
    if type(data) == dict: data = [data]
    if data:
	if actiontype == 'disk':
            data = map(cleanupdata, data)
        t = PrettyTable(data[0].keys())
        for each in data:
            t.add_row(each.values())
        return t
    return ''
# -------------------------------------------------------------------------

if __name__ == "__main__":
    parser = ArgumentParser("awsr2c api for get usage data")
    parser.add_argument('--awsr2cApiHost', help='awsr2c api hostname/IP',required=True)
    parser.add_argument('--skyIP', help='Sky IP address',required=True)
    parser.add_argument('--skyUser', help='Sky username, not required if sessionID provided')
    parser.add_argument('--skyVenderkey', help='Sky vender key, not required if sessionID provided')
    parser.add_argument('--sessionID', help='UUID returned after successful login into Sky')
    parser.add_argument('--action', help='disk/aggregateDisk/dedup/snapshot/mdl/backup/protectedHost/protectedAppSize/protectedHostCount/login(to retrieve sessionID)',required=True)
    parser.add_argument('--startDate', help='ex- 2015-07-01(optional)', default=CURRENT_DATE)
    parser.add_argument('--hostName', help='host name(optional)', default='*')
    parser.add_argument('--appName', help='app name(optional)', default='*')
    args = parser.parse_args()
    args = vars(args)
    if not args.get('sessionID'):
        pswd = getpass.getpass('Enter your sky password:')
        args.update({"skyPassword":pswd})
    response = main(args)
    if args.get('action') != 'login':
       print make_table(response, args.get('action'))

# -------------------------------------------------------------------------


