'''
Created on Jun 3, 2015

@author: chandra.mishra
'''

import sys
import traceback

class Awsr2cError(Exception):


    def __init__(self,  msg=None, code=None, data=None, stacktrace=None):
        super(Exception, self).__init__(msg)
        self.code = code
        self.msg = msg
        self.data = data
        self.stacktrace = stacktrace

    def __str__(self):
        if self.stacktrace:
            exc_type, exc_value, exc_tb = sys.exc_info();
            self.stacktrace=traceback.format_exception(exc_type, exc_value, exc_tb)
            return 'awsr2cError: code=%s, msg=%s, data=%s, stacktrace=%s' % (self.code, self.msg, self.data, self.stacktrace)
        else:
            return 'awsr2cError: code=%s, msg=%s, data=%s' % (self.code, self.msg, self.data)

