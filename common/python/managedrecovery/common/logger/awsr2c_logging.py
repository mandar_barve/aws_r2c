'''
Created on Jun 3, 2015

@author: chandra.mishra
'''

import socket
import inspect
from logging import getLogger

class Awsr2cLogger():

    # -----------------------------------------------------------------------
    SEVERITY_CRITICAL = (1, "Critical")
    SEVERITY_ERROR = (2, "Error")
    SEVERITY_WARNING = (3, "Warning")
    SEVERITY_INFO = (4, "Info")
    SEVERITY_DEBUG = (5, "Debug")

    LIFECYCLE_INIT = (1, 'Initialization')
    LIFECYCLE_SERVICE = (2, 'InService')
    LIFECYCLE_FINAL = (3, 'Finalization')

    # -------------------------------------------------------------------------
    def __init__(self, logger, component):
        self.logger = logger
        self.host = socket.gethostname()
        self.component = component

    # -------------------------------------------------------------------------
    def _merge_lines(self, stack):
        if stack is None or stack == "":    return stack
        splitted = stack.splitlines()
        if len(splitted) == 1:   return stack
        return "\\n".join(splitted)

    # -------------------------------------------------------------------------
    def _find_caller(self):
        try:
            call_stack = inspect.stack()
            caller = call_stack[3]
            # print caller[1], caller[2], caller[3]
            return caller
        except:
            return ['', '', '', '']

    # -------------------------------------------------------------------------
    def _construct_extra(self, severity, stack, action):

        caller = self._find_caller()
        module_name = caller[3]
        if module_name.find('__init__') >= 0:  life_cycle = self.LIFECYCLE_INIT
        elif module_name.find('finalize') >= 0:  life_cycle = self.LIFECYCLE_FINAL
        else:   life_cycle = self.LIFECYCLE_SERVICE

        stack = self._merge_lines(stack)
        extra = {'host':self.host, 'stack':stack, 'cpathname':caller[1], 'clineno':caller[2], 'cfuncName':caller[3]}
        extra['component'] = self.component[1]
        extra['compid'] = self.component[0]
        extra['severity'] = severity[1]
        extra['sevid'] = severity[0]
        extra['lifecycle'] = life_cycle[1]
        extra['lsid'] = life_cycle[0]
        extra['action'] = action

        return extra

    # -------------------------------------------------------------------------
    def info(self, msg, stack="", action=""):
        msg = msg.encode('utf-8')
        extra = self._construct_extra(self.SEVERITY_INFO, stack, action)
        self.logger.info(msg, extra=extra)

    # -------------------------------------------------------------------------
    def debug(self, msg, stack="", action=""):
        msg = msg.encode('utf-8')
        extra = self._construct_extra(self.SEVERITY_DEBUG, stack, action)
        self.logger.debug(msg, extra=extra)

    # -------------------------------------------------------------------------
    def warning(self, msg, stack="", action=""):
        msg = msg.encode('utf-8')
        extra = self._construct_extra(self.SEVERITY_WARNING, stack, action)
        self.logger.warning(msg, extra=extra)

    # -------------------------------------------------------------------------
    def error(self, msg, stack="", action=""):
        msg = msg.encode('utf-8')
        extra = self._construct_extra(self.SEVERITY_ERROR, stack, action)
        self.logger.error(msg, extra=extra)

    # -------------------------------------------------------------------------
    def critical(self, msg, stack="", action=""):
        msg = msg.encode('utf-8')
        extra = self._construct_extra(self.SEVERITY_CRITICAL, stack, action)
        self.logger.critical(msg, extra=extra)

    # -------------------------------------------------------------------------
    def log(self, msg, stack="", action=""):
        self.info(msg, stack, action)
    # -------------------------------------------------------------------------


class Awsr2clogging():

    COMPONENT_ACTIFIO = 'ACTIFIO'
    COMPONENT_LINUX_COMMAND_INTERFACE = 'LINUX COMMAND'
    COMPONENT_WINDOWS_COMMAND_INTERFACE = 'WINDOWS COMMAND'
    COMPONENT_API = 'API'

    # -------------------------------------------------------------------------
    @classmethod
    def getLogger(cls, name, component):
        logger = getLogger(name)
        aws_logger = Awsr2cLogger(logger, component)
        return aws_logger
    # -------------------------------------------------------------------------
